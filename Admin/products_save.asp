﻿<!--#include file="top.asp"-->
<!--#include file="../inc/function.asp"-->
<!--#include file="../inc/indexSub.asp"-->
<!--#include file="../inc/productSub.asp"-->
<%
dim founderr,errmsg
founderr=false
i=0
errmsg=""
dim p_name,p_name_e,p_spec,p_spec_e,p_type,p_type_e,p_type_id,p_order,p_pic,p_pic2,p_jianjie,p_jianjie_e,id
dim action,rs
p_name=checkstr(request.Form("p_name"))
p_name_e=checkstr(request.Form("p_name_e"))
p_spec=checkstr(request.Form("p_spec"))
p_spec_e=checkstr(request.Form("p_spec_e"))
p_price=checkstr(request.Form("p_price"))
p_scprice=checkstr(request.Form("p_scprice"))
p_type_id=request.Form("p_type_id")
p_order=request.Form("p_order")
p_pic=checkstr(request.Form("p_pic"))
p_pic2=checkstr(request.Form("p_pic2"))
p_pic3=checkstr(request.Form("p_pic3"))
p_pic4=checkstr(request.Form("p_pic4"))
p_pic5=checkstr(request.Form("p_pic5"))
p_jianjie=checkstr(request.Form("p_jianjie"))
p_jianjie_e=request.Form("p_jianjie_e")
p_epitome=request.Form("p_epitome")



id=request("id")
action=request("action")

if action="new" or action="edit" then
	if not chkRange(p_name,2,50) then
		founderr=true
		i=i+1
		errmsg=errmsg&i&"),你必须输入产品的中文标题！"	
	End if	
	if not chkRange(p_spec,2,30) then
		founderr=true
		i=i+1
		errmsg=errmsg&i&"),你必须输入产品的中文型号！"	
	End if
	if not chkrequest(p_price) then
		founderr=true
		i=i+1
		errmsg=errmsg&i&"),你必须输入产品的售价！"	
	End if
	if not chkRange(p_pic,10,150) then
		founderr=true
		i=i+1
		errmsg=errmsg&i&"),你没有上传产品的小图！"	
	End if	
	if not chkrequest(p_type_id) then
		founderr=true
		i=i+1
		errmsg=errmsg&i&"),你没有选择产品的分类！"	
	else
		sql="select p_type,p_type_e from p_class where p_id="&p_type_id
		set rs=server.CreateObject("adodb.recordset")
		rs.open sql,conn,1,1
		if rs.bof and rs.eof then
			founderr=true
			i=i+1
			errmsg=errmsg&i&"),你没有正确选择产品的分类！"
		else
			p_type=rs("p_type")
			p_type_e=rs("p_type_e")
		end if
		rs.close
		set rs=nothing
	end if
	if chkNull(p_epitome,10) then
		founderr=true
		i=i+1
		errmsg=errmsg&i&"),你没有输入产品的中文介绍！"	
	End if	
end if

if action="edit" or action="del" then
	if not chkrequest(id) then alert "error","",1
end if

if founderr then alert errmsg,"",1

if action="new" then
	sql="select * from p_info"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,conn,1,3
	rs.addnew
	rs("p_date")=now()
	rs("p_name")=p_name
	rs("p_name_e")=p_name_e
	rs("p_spec")=p_spec
	rs("p_spec_e")=p_spec_e
	rs("p_price")=p_price
	rs("p_scprice")=p_scprice
	rs("p_order")=p_order
	rs("p_pic")=p_pic
	rs("p_pic2")=p_pic2
	rs("p_pic3")=p_pic3
	rs("p_pic4")=p_pic4
	rs("p_pic5")=p_pic5
	rs("p_type_id")=p_type_id
	rs("p_type")=p_type
	rs("p_type_e")=p_type_e
	rs("p_jianjie")=p_jianjie
	rs("p_epitome")=replace_text(p_epitome)
	'rs("p_jianjie_e")=replace_text(p_jianjie_e)
	rs.update	
	id=rs("p_id")
	if err then
		response.Write(err.description)
		response.End()
	end if
	rs.close
End if

if action="edit" then
	sql="select * from p_info where p_id="&id
	set rs=server.createobject("adodb.recordset")
	rs.open sql,conn,1,3
	if not (rs.bof and rs.eof) then
		rs("p_name")=p_name
		rs("p_name_e")=p_name_e
		rs("p_spec")=p_spec
		rs("p_spec_e")=p_spec_e
		rs("p_price")=p_price
		rs("p_scprice")=p_scprice
		rs("p_order")=p_order
		rs("p_pic")=p_pic
		rs("p_pic2")=p_pic2
		rs("p_pic3")=p_pic3
		rs("p_pic4")=p_pic4
		rs("p_pic5")=p_pic5
		rs("p_type_id")=p_type_id
		rs("p_type")=p_type
		rs("p_type_e")=p_type_e
		rs("p_jianjie")=p_jianjie
		rs("p_epitome")=replace_text(p_epitome)
		rs.update	
	end if
	rs.close
End if
if (action="new" or action="edit") and cityviewmode=1 then
	
	tarr=savetohtml(getproduct_viewpage(id),"/products/"&id&".html")
	tarr2=savetohtml(getindexpage(),"/index.html")

	tarr=savetohtml(geteproduct_viewpage(id),"/eproducts/"&id&".html")
	tarr2=savetohtml(getdefaultpage(),"/default.html")
	call createSeoPage()
	if tarr(0)<>0 then alert tarr(1),"",1
end if
if action="del" then

sql="select p_pic,p_pic2,p_pic3,p_pic4,p_pic5,p_epitome,p_jianjie,p_jianjie_e from p_info where p_id="&id
set rs=server.createobject("adodb.recordset")
rs.open sql,conn,1,3
if Not(rs.BOF or rs.EOF) then
	if not chkNull(rs("p_pic"),10) then call delfile(rs("p_pic"))
	if not chkNull(rs("p_pic2"),10) then call delfile(rs("p_pic2"))
	if not chkNull(rs("p_pic3"),10) then call delfile(rs("p_pic3"))
	if not chkNull(rs("p_pic4"),10) then call delfile(rs("p_pic4"))
	if not chkNull(rs("p_pic5"),10) then call delfile(rs("p_pic5"))
	if not chkNull(rs("p_epitome"),10) then call delContentPic(rs("p_epitome"))	
rs.delete
call delfile("/products/"&id&".html")
call delfile("/eproducts/"&id&".html")
if cityviewmode=1 then
	call savetohtml(getindexpage(),"/index.html")
	call savetohtml(getdefaultpage(),"/default.html")
end if
call createSeoPage()
End if
rs.close
End if
set rs=nothing
closeconn
response.Redirect("products.asp")
%>