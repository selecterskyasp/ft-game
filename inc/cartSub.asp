<%
function savetocart(pid,num)
dim st,tst
dim tarr,i
dim t,tpid,tnum,tarr2
dim isfind

idfind=false
st=checkstr(request.Cookies("st"))
tst=""
dim trs,cartnum,cartmoney
set trs=server.CreateObject("adodb.recordset")
sql="select p_price from p_info where p_id=" & pid
trs.open sql,conn,1,1
if trs.eof then
	trs.close
	set trs=nothing
	exit function
end if

cartnum=request.Cookies("cartnum")
cartmoney=replace(request.Cookies("cartmoney"),",","")
if not chkrequest(cartnum) then cartnum=0
if not chkrequest(cartmoney) then cartmoney=0
cartnum=cartnum+num

cartmoney=getprice(cdbl(trs("p_price")) * num,2)
trs.close
set trs=nothing
response.Cookies("cartnum")=cartnum
response.Cookies("cartmoney")=cartmoney

if st="" or instr(st,",")=0 then
	tst=pid & "," & num
else
	tarr=split(st,"|")
	for i=0 to ubound(tarr)
		t=tarr(i)
		if chkpara(t)then
			tarr2=split(t,",")
			tpid=tarr2(0)
			tnum=clng(tarr2(1))
			
			if tpid=pid then
				tnum=tnum+num
				if tst="" then tst=pid & "," & tnum else tst= tst & "|" & pid & "," & tnum
				isfind=true
				
			else
				if tst="" then tst=tpid & "," & tnum else tst= tst & "|" & tpid & "," & tnum
			end if
			
			erase tarr2
		end if		
	next
	erase tarr
	if not isfind then
		if tst="" then tst=pid & "," & num else tst= tst & "|" & pid & "," & num
	end if
end if
response.Cookies("st")=tst

tst="":st=""
end function

function delcart(pid)
dim st,tst
dim tarr,tarr2,i,t,tpid,tnum
dim trs,cartnum,cartmoney,tprice
set trs=server.CreateObject("adodb.recordset")
sql="select p_price from p_info where p_id=" & pid
trs.open sql,conn,1,1
if trs.eof then
	trs.close
	set trs=nothing
	exit function
else
	tprice=cdbl(trs("p_price"))
end if
trs.close
set trs=nothing

cartnum=request.Cookies("cartnum")
cartmoney=replace(request.Cookies("cartmoney"),",","")
if not chkrequest(cartnum) then cartnum=0
if not chkrequest(cartmoney) then cartmoney=0

delcart=false
st=checkstr(request.Cookies("st"))
if st="" or instr(st,",")=0 then exit function
tarr=split(st,"|")
tst=""
for i=0 to ubound(tarr)
	t=tarr(i)
	if chkPara(t) then
		tarr2=split(t,",")
		tpid=tarr2(0)
		tnum=clng(tarr2(1))
		'删除已经存在的产品
		if tpid<>pid then
			if tst="" then tst=tpid & "," & tnum else tst=tst & "|" & tpid & "," & tnum
		else
			if cartnum>tnum then cartnum=cartnum-tnum else cartnum=0
			if cartmoney>tprice*tnum then cartmoney=cartmoney-tprice*tnum else cartmoney=0
		end if
		
		erase tarr2
	end if
next
erase tarr
response.Cookies("st")=tst
response.Cookies("cartnum")=cartnum
response.Cookies("cartmoney")=cartmoney
delcart=true
end function

function updatecart()
dim ids,nums
dim tarr,tarr2,i
dim tst,trs
dim ttnum,ttmoney
ttnum=0:ttmoney=0
tst=""
ids=replace(checkstr(request.Form("ids"))," ","")
nums=replace(checkstr(request.Form("nums"))," ","")
'response.Write(ids)
'response.End()

if not chknull(ids,1) then
	tarr=split(ids,",")
	tarr2=split(nums,",")
	set trs=server.CreateObject("adodb.recordset")
	for i=0 to ubound(tarr)
		sql="select p_price from p_info where p_id=" & tarr(i)
		
		trs.open sql,conn,1,1
		if not trs.eof then
			ttnum=ttnum+clng(tarr2(i))
			ttmoney=ttmoney+cdbl(trs("p_price"))*tarr2(i)
			if tst="" then tst=tarr(i) & "," & tarr2(i) else tst=tst & "|" & tarr(i) & "," & tarr2(i)
		end if
		trs.close
	next
	set trs=nothing
	erase tarr
	erase tarr2
end if

response.Cookies("st")=tst
response.Cookies("cartnum")=ttnum
response.Cookies("cartmoney")=ttmoney
end function

function cartlist()
dim st
dim tarr,tarr2,i,t,tpid,tnum
dim trs,tcontent
dim turl,tname,p_spec,p_pic,p_price
st=checkstr(request.Cookies("st"))
ttnum=0
if st="" or instr(st,",")=0 then
	cartlist=""
	exit function
end if
set trs=server.CreateObject("adodb.recordset")
tarr=split(st,"|")
tcontent=""

for i=0 to ubound(tarr)
	t=tarr(i)
	if chkPara(t) then
		tarr2=split(t,",")
		sql="select * from p_info where p_id=" & tarr2(0)
		trs.open sql,conn,1,1
		if not trs.eof then		
			if cityviewmode=0 then
				turl="product_view.asp?id=" & trs("p_id")
			else
				turl="product/" & trs("p_id") & ".html"
			end if
			tname=getinnertext(trs("p_name"))
			p_pic=trs("p_pic")
			p_spec=getinnertext(trs("p_spec"))
			p_price=getprice(trs("p_price"),2)
			ttnum=ttnum+tarr2(1)*cdbl(trs("p_price"))
			
			
			tcontent = tcontent & "<TR>" & vbcrlf & _
				"    <TD  align=middle><A " & vbcrlf & _
				"      href=""cart.asp?act=del&id=" & tarr2(0) & """><IMG " & vbcrlf & _
				"      title=Delete height=12 alt=Delete src=""" & citymodepath & "/images/del.gif"" width=12 " & vbcrlf & _
				"      border=0></A></TD>" & vbcrlf & _
				"    <TD  align=middle>" & vbcrlf & _
				"<input type=""hidden"" name=""ids"" value=""" & tarr2(0) & """ /><INPUT class=""textbox"" " & vbcrlf & _
				"      style=""TEXT-ALIGN: center"" size=""2"" value=""" & tarr2(1) & """ name=""nums""></TD>" & vbcrlf & _
				"    <TD  align=middle><IMG title="""" alt="""" " & vbcrlf & _
				"      src="""&p_pic&""" width=""75"" height=""50""></TD>" & vbcrlf & _
				"    <TD >" & tname & "</TD>" & vbcrlf & _
				"    <TD  align=middle>"&p_spec&"</TD>" & vbcrlf & _				
				"    <TD  align=right>$" & p_price & "</TD>" & vbcrlf & _
				"    <TD  align=right width=80>$" & getprice(cdbl(trs("p_price")) * tarr2(1),2) & "</TD></TR>" & vbcrlf & _
			""

		end if
		trs.close
		erase tarr2
	end if
next
erase tarr
set trs=nothing
cartlist=tcontent:tcontent=""
end function
%>