<%
function getproductspage()
dim tbody
dim k,d,t
dim id,st
dim tmparr,classname
id=request.QueryString("id")
st="":classname=""
if chkrequest(id) then 
	sql="select p_type from p_class where p_id="&id
	tmparr=getmdbvalue(sql)
	if tmparr(0)=1 then
	st=st&" and  p_type_id="&id
	classname=tmparr(1)
	end if
	erase tmparr
end if
k=checkStr(request.QueryString("k"))
if len(k)>1 and len(k)<20 then
	st=" and p_name like '%"&k&"%'"
end if
dim pagesize,pagecount,recordcount,page
PageSize=12   
'获得总数  
sql="select count(p_id) from p_info where p_id>0"&st
recordcount=conn.execute(sql)(0)
'总页数  
if cint(recordcount) = 0 then 
	pagecount=1
else
	pagecount=Abs(Int(recordcount/PageSize*(-1)))   
end if
'获得当前页码   
page=request.QueryString("page")
if not chkrequest(page) then page=1 else page=cint(page)
if page>pagecount then page=pagecount
k=cityname&","&classname&""
d=cityname&""&classname&""
t=cityname&"product sales_guangzhou"&classname&""
tbody=getcache("/products.html")
tbody=replace(tbody,"$head$",gethead(k,d,t))
tbody=replace(tbody,"$left$",getleft(0,1))
tbody=replace(tbody,"$id$",id)
tbody=replace(tbody,"$getproducts$",getproducts(pagesize,st,2,0))
tbody=replace(tbody,"$pages$",showpage(pagecount,pagesize,page,recordcount,10))
tbody=replace(tbody,"$left$",getleft(0,0))
tbody=replace(tbody,"$foot$",getfoot(0))
getproductspage=tbody:tbody=""
end function

function getproduct_viewpage(sid)
dim thead,tbody
dim k,d,t
dim rsPro,pic
dim pre,nex,pret,nextt,i,biglist
strsql="select * from p_info where p_id="&sid
set rsPro = Server.CreateObject("ADODB.Recordset")
rsPro.Open strsql, conn, 1, 1
if rsPro.bof and rsPro.eof then
	call closers(rsPro)
	geteproduct_view="false"
	exit function
end if
k=rsPro("p_name")&","&rsPro("p_type")&","&cityname&""
d=leftt(rsPro("p_jianjie"),100)
t=rsPro("p_name")&" "&rsPro("p_type")&" "&cityname&" "

tbody=getcache("/product_view.html")
pic=rsPro("p_pic2")
if chknull(pic,5) then
	pic=rsPro("p_pic")
end if
'检测大图
biglist=""
for i=3 to 5
	if not chknull(rsPro("p_pic" & i),10) then biglist= biglist & "<li><img src=""" & rsPro("p_pic" & i) & """ onclick=""chage(this.src)"" onload=""cjx.pic_reset(this,'80,80')"" /></li>" & vbcrlf
next

if left(pic,1)<>"/" then pic="/"&pic
tbody=replace(tbody,"$head$",gethead(k,d,t))
tbody=replace(tbody,"$left$",getleft(0,1))
tbody=replace(tbody,"$id$",sid)
tbody=replace(tbody,"$classid$",rsPro("p_type_id"))
tbody=replace(tbody,"$classname$",rsPro("p_type"))
tbody=replace(tbody,"$pic$",pic)
tbody=replace(tbody,"$biglist$",biglist)
tbody=replace(tbody,"$p_name$",rsPro("p_name"))
tbody=replace(tbody,"$p_spec$",rsPro("p_spec"))
tbody=replace(tbody,"$price$",getprice(rsPro("p_price"),2))
tbody=replace(tbody,"$p_epitome$",replace_t(rsPro("p_epitome")))
rsPro.close
set rspro=nothing
tbody=replace(tbody,"$left$",getleft(0,0))
tbody=replace(tbody,"$foot$",getfoot(0))
getproduct_viewpage=tbody:tbody="":thead=""
end function


'snum条数 swhere附件条件 ssort排序 slist 0首页推荐 1首页最新 2 分页 3首页流行
function getProducts(snum,swhere,slist,slanguage)
dim trs,i
dim tt,turl,tpic,tdate,tcontent,scprice,price,jianjie
dim tpage
if slist=0 then
sql="select top "&snum&" p_id,p_name,p_pic,p_name_e,p_scprice,p_price from p_info order by p_order desc"
elseif slist=3 then
sql="select top "&snum&" p_id,p_name,p_pic,p_name_e,p_scprice,p_price from p_info where p_flag=1 order by p_date desc"
else
	tpage=request.QueryString("page")
	if chkrequest(tpage) then tpage=cint(tpage) else tpage=0
	if tpage>1 then
		sql="SELECT TOP "&snum&" p_id,p_name,p_pic,p_name_e,p_scprice,p_price,p_jianjie from p_info where (p_id <(SELECT MIN(p_id) FROM (SELECT TOP "&((tpage-1)*snum)&" p_id FROM p_info where p_id>0"&sWhere&" order by p_id desc) AS tblTMP)) and p_id>0"&sWhere&" order by p_id desc"
	else
	sql="select top "&snum&" p_id,p_name,p_pic,p_name_e,p_scprice,p_price,p_jianjie from p_info where p_id>0"&swhere&" order by p_id desc"
	end if
end if
set trs=server.CreateObject("adodb.recordset")
trs.open sql,conn,1,1
if not (trs.eof and trs.bof) then
	i=0
	
	do while not trs.eof and i<snum
		tid=trs("p_id")
		scprice=getprice(trs("p_scprice"),2)
		price=getprice(trs("p_price"),2)
		if slanguage=0 then	
			tt=trs("p_name")
			if cityviewmode=0 then 
				turl="/product_view.asp?id="&tid
			else
				turl="/products/"&tid&".html"
			end if
		else 
			tt=trs("p_name_e")
			if cityviewmode=0 then
				turl="/eproduct_view.asp?id="&tid
			else
				turl="/eproducts/"&tid&".html"
			end if
		end if
		tpic=trs("p_pic")
		if left(tpic,1)<>"/" then tpic="/"&tpic
		select case slist
		case 0
			tcontent=tcontent&"<li><a href="""&turl&""" title="""&tt&""" target=""_blank"">"&left(tt,20)&"</a><span>Save $" & price & " </span></li>" & vbcrlf
		case 3
				
			tcontent=tcontent&"<li><a href="""&turl&""" title="""&tt&""" target=""_blank"">"&left(tt,20)&"</a></li>" & vbcrlf 		
			
		case 1
			tcontent =tcontent &	"<li>" & vbcrlf & _
	"        <p class=""title""><a href="""&turl&""" title="""&tt&""" target=""_blank"">"&left(tt,20)&"</a></p>" & vbcrlf & _
	"        <p><a href="""&turl&""" title="""&tt&"""><img src="""&tpic&""" alt="""&tt&""" title="""&tt&""" onload=""cjx.pic_reset(this,'75,50');"" /></a>" & vbcrlf & _
	"        <p>$" & price & "</p>" & vbcrlf & _
	"        <br /> <br /> <br />" & vbcrlf & _
	"        <p class=""buy""><a href=""savetocart.asp?id=" & tid & """>Buy</a></p>" & vbcrlf & _
	"        <p class=""buy""><a href="""&turl&""">See More</a></p>" & vbcrlf & _
	"        </li>" & vbcrlf 

		case 2
		jianjie=trs("p_jianjie")
			tcontent=tcontent &	"<li>" & vbcrlf & _
	"           <p class=""productlist2title""><a href=""" & turl & """ title="""&tt&""" target=""_blank"">"&left(tt,20)&"</a></p>" & vbcrlf & _
	"            <div class=""productcontent"">" & vbcrlf & _
	"            <ul>" & vbcrlf & _
	"            	<dl class=""l1""><a href=""" & turl & """><img src="""&tpic&"""/></a></dl>" & vbcrlf & _
	"                <dl class=""l2""><a href=""" & turl & """>" & left(jianjie,100) & "…</a></dl>" & vbcrlf & _
	"      			<dl class=""l3"">" & vbcrlf & _
	"                   <!-- <p class=""gray bold"">OUT OF STOCK </p>-->" & vbcrlf & _
	"                    <p class=""gray bold"">Price: <span class=""del"">$" & scprice & "</span></p>" & vbcrlf & _
	"                    <!--<p class=""red bold"">$284.95 <span class=""gray del""> - $399.95</span></p>-->" & vbcrlf & _
	"                    <p class=""red"">$" & price & "</p>" & vbcrlf & _
	"                    <p><a href=""" & turl & """ class=""txtButton"">MORE INFO</a></p>" & vbcrlf & _
	"                </dl>" & vbcrlf & _
	"            </ul>" & vbcrlf & _
	"            </div>" & vbcrlf & _
	"       </li>" & vbcrlf 
		end select
		trs.movenext
		i=i+1		
	loop
	
else
	tcontent="No Content"
end if
trs.close
set trs=nothing
getProducts=tcontent
tcontent=""
end function
%>
