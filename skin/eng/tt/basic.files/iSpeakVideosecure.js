
/* iSpeakVideo class
 * This code is provided as isCopyrighted 2007 Bambit Technolgoies, Inc.
 * and licensed to iSpeakVideo
 * Unauthorized reproduction is illeagal
 * This copyright notice must remain intact.
 */
var iSpeakVideo_Global;
function hideWhenFinished() {
	iSpeakVideo_Global.HideWhenFinished();
}

function iSpeakVideo() {
	this.src="";
	this.width="";
	this.height="";	
	this.Position
	this.Location=new this.Point();
	this.Location.X=0;
	this.Location.Y=0;
	this.DoNotShowDays=0;
	this.PauseIfShown=0;
	this.PositionMode='absolute';
	this.positionFrom='fromBottom';
	this.positionHorizontalFrom='fromLeft';
	this.ReferenceID=null;
	this.RequiredVersionMajor=9;
	this.RequiredVersionMinor=0;
	this.needFlashString="Flash Version "+this.RequiredVersionMajor+"."+this.RequiredVersionMinor+" is needed.<br/><a href='https://www.adobe.com/go/getflash/' target='_blank'>Get it here</a>";
	if(document.body.attachEvent) { this.isIE=true;} else { this.isIE=false;}
	this.ParseBrowser();
	this.backgroundImage='images/transparent.gif';
	this.NeedsFix=false;
	this.WriteWrapper=true;
	this.FlashVars='';
	this.urlArray=null;
	this.anchorMode='static';
	this.InitialX=0;
	this.cookie=null;
	this.hideAfterMaxPlays=0;
	this.playCountDays=null;
	this.playCountCookie=null;
	this.maxPlayCount=0;
}
iSpeakVideo.MouseX=function( e ) {


	return e.pageX || ( e.clientX + ( document.documentElement.scrollLeft || document.body.scrollLeft ) );
}
iSpeakVideo.MouseY=function ( e ) {
	return e.pageY|| ( e.clientY + ( document.documentElement.scrollTop|| document.body.scrollTop ) );
}



iSpeakVideo.prototype.LockMouse=function() {
	
	this.MouseStartX=null;
	this.MouseStartY=null;
	if(this.isIE) {		
		
		document.attachEvent('onmousemove', iSpeakVideo.TrackMouse);
		document.attachEvent('onmouseup', iSpeakVideo.ReleaseMouse);
	} else {
		document.addEventListener('mousemove', iSpeakVideo.TrackMouse,false);
		document.addEventListener('mouseup', iSpeakVideo.ReleaseMouse,false);
	}
}
iSpeakVideo.prototype.GetMovie=function() {
	var ie = navigator.appName.indexOf("Microsoft") != -1;
	return (ie) ? window['iSpeakVideo_Container_o'] : document['iSpeakVideo_Container_o'];
}
iSpeakVideo.prototype.PausePlayback=function()  {
	var movie=this.GetMovie();
	movie.pausePlayback();
}



iSpeakVideo.prototype.trace=function(t) {
	alert(t);
}
iSpeakVideo.ReleaseMouse=function(e) {
	
	if(document.body.attachEvent)	{

		
		document.detachEvent('onmousemove', iSpeakVideo.TrackMouse);
		document.detachEvent('onmouseup', iSpeakVideo.ReleaseMouse);
	} else {
	document.removeEventListener('mousemove', iSpeakVideo.TrackMouse,false);
	document.removeEventListener('mouseup', iSpeakVideo.ReleaseMouse,false);
	}
}
iSpeakVideo.TrackMouse=function(e) {
	if(iSpeakVideo.MouseStartX!=null)  {
		var curX=iSpeakVideo.MouseX(e);
		var curY=iSpeakVideo.MouseY(e);
		var diffX=curX-iSpeakVideo.MouseStartX;
		var diffY=curY-iSpeakVideo.MouseStartY;
		var obj=iSpeakVideo_Global.el('iSpeakVideo_Container');
		var src=iSpeakVideo_Global.el(iSpeakVideo_Global.ReferenceID);
		var srcX=iSpeakVideo_Global.GetObjectAbsoluteX(obj);
		
		var srcY=iSpeakVideo_Global.GetObjectAbsoluteY(obj);	
		srcX+=diffX;
		srcY+=diffY;
		obj.style.left=srcX + 'px';
		obj.style.top=srcY + 'px';
	}	
	iSpeakVideo.MouseStartX=iSpeakVideo.MouseX(e);
	iSpeakVideo.MouseStartY=iSpeakVideo.MouseY(e);
}

iSpeakVideo.prototype.Query=function(x) {
	if(this.urlArray==null) {
		var res=new Array();
		var href=new String(window.location.href);
		var qi=href.indexOf('?');
		if(qi!=-1) {
			var tail=new String(href.substr(qi+1));
			var params=tail.split('&');
			for(var i=0;i<params.length;i++) {
				if(params[i].indexOf('=')!=-1) {
					var split=params[i].split('=');
					res[split[0].toLowerCase()]=split[1];
				} else {
						res[params[i].toLowerCase()]='';
					}
				}
			}
		this.urlArray=res;
	}
	return this.urlArray[x];
}

iSpeakVideo.prototype.AddUrlVarToFlashVars=function(a) {
	this.AddVarToFlashVars(a,this.Query(a));
} 

iSpeakVideo.prototype.AddVarToFlashVars=function(n,v) {
	this.FlashVars=this.FlashVars+n+'='+v+'&';
}

iSpeakVideo.prototype.AddUrlVarToFlashVarsRenamed=function(a,name) {
	this.FlashVars=this.FlashVars+name+'='+this.Query(a)+'&';
}
iSpeakVideo.prototype.Point=function(x,y){
	this.X=x;
	this.Y=y;
}

iSpeakVideo.prototype.GetTimeCookie=function() {
	this.cookie= this.gc("isvTime");
	return this.cookie;
}

iSpeakVideo.prototype.GetPlayCookie=function() {
	this.playCountCookie= parseInt(this.gc("playCount"));
	if(isNaN(this.playCountCookie)) { this.playCountCookie=0;}
	
	return this.playCountCookie;
}


iSpeakVideo.prototype.cc=function(n,v,d) {
	if (d) {
		var dt = new Date();
		dt.setTime(dt.getTime()+(d*24*3600000));
		var expires = "; expires="+dt.toGMTString();
	}
	else var expires = "";
	document.cookie = n+"="+v+expires+"; path=/";
	
}



iSpeakVideo.prototype.gc=function(n) {
	var ne = n + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(ne) == 0) return c.substring(ne.length,c.length);
	}
	return null;
}

iSpeakVideo.prototype.ec=function(name) {
	createCookie(name,"",-1);
}


iSpeakVideo.prototype.SetPlayCookie=function() {
	
	var count=this.GetPlayCookie();
	
	if(isNaN(this.playCountCookie)) { count=1;}
	else {count=this.playCountCookie+1;}
	
	this.cc("playCount",count,this.playCountDays);
}


iSpeakVideo.prototype.SetTimeCookie=function() {
	this.cc("isvTime",this.DoNotShowDays);
}

iSpeakVideo.prototype.ParseBrowser=function() {
	this.isNetscape=false;
	this.isFirefox=false;
	this.isIE=false;
	this.isOpera=false;
	this.isSafari=false;
	this.appName=navigator.appName;
	this.appVersion=navigator.appVersion;
	if(this.appName=="Netscape"&&parseFloat(this.appVersion)>4.7) {
		var uai=navigator.userAgent.indexOf("Firefox");
		if(uai!=-1) {
			this.isFirefox=true
			this.browserVersionMaj=parseInt(navigator.userAgent.charAt(uai+8));
		} else {
			this.isNetscape=true;
			this.browserVersionMaj=parseFloat(this.appVersion);
		}
	} else {
		var uai=navigator.appVersion.indexOf("MSIE");
		if(uai!=-1) {
			this.isIE=true;
			var temp=navigator.appVersion.split("MSIE")
			this.appVersion=parseFloat(temp[1]);
			this.browserVersionMaj=parseInt(this.appVersion);
		} else {
			var uai=navigator.userAgent.indexOf("Opera");
			if(uai!=-1){
				this.isOpera=true;
				this.browserVersionMaj=parseInt(navigator.userAgent.charAt(uai+6));
			}
		}
	}
	if(document.compatMode=='CSS1Compat') {
		this.isCompatible=true;
	} else {
		this.isCompatible=false;
	}
}
iSpeakVideo.prototype.el=function(e) { return document.getElementById(e);}
iSpeakVideo.prototype.AnchorToObjectX=function(){
	if(this.isIE) {		
		window.attachEvent('onresize',AnchorResizedX);
		window.attachEvent('onload',AnchorResizedX);
	} else {
		window.addEventListener('load',AnchorResizedX,false);
		window.addEventListener('resize',AnchorResizedX,false);
	}
}

iSpeakVideo.prototype.IsToBePaused=function() {
	if(this.DoNotShowDays>0){
		if(this.cookie!=null) {
			return true;
		} 
	}
	if(this.maxPlayCount>0) {
		
		this.GetPlayCookie();
		
		if(this.playCountCookie != null && this.playCountCookie>=this.maxPlayCount) {
			return true;
		}
		this.SetPlayCookie();
	}
	return false;
}

iSpeakVideo.prototype.Intialize=function() {
	iSpeakVideo_Global=this;
	cookie=this.GetTimeCookie();
	//	Before we go further, check our cookie values
	
	if(this.DoNotShowDays>0){	
		this.GetTimeCookie();
			if(this.cookie!=null && this.PauseIfShown==0) {
		return true;
		} 
		this.SetTimeCookie();
	}
	if(this.hideAfterMaxPlays >0) {
		this.GetPlayCookie();
		
		if(this.playCountCookie != null && this.playCountCookie>=this.maxPlayCount) {
			return true;
		}
		
		this.SetPlayCookie();
	}
	this.InitialX=this.Location.X;

		if(this.ReferenceID!=null && this.el(this.ReferenceID)!=null) {
			this.AnchorToObjectX();
			this.LockInitialX();
		}
	if(this.anchorMode=='static')
	{
		this.positionMode='fixed';
		if(this.isIE) {
			if((this.appVersion!=7 && this.appVersion!=8) || !this.isCompatible)	{
				this.positionMode='absolute';
				this.NeedsFix=true;
			}
		}
		if(this.isFirefox && this.browserVersionMaj<2) {
			this.positionMode='absolute';
			window.addEventListener('scroll',RestorePosition,false);
			window.addEventListener('size',RestorePosition,false);
	
		}
		
		this.WriteDiv();
	} else {
		this.positionMode='absolute';
		this.WriteDiv();
	}
		if(this.ReferenceID!=null && this.el(this.ReferenceID)!=null) {
			this.AnchorToObjectX();
		}
	
	
}
iSpeakVideo.prototype.PlaceElement=function (o,s,l,t) {
	var obj=this.el(o); var src=this.el(s);
	var srcLoc=GetObjectPosition(src);
	var nl=l+srcLoc[0];
	var nt=t+srcLoc[1];
	obj.style.left=nl + 'px';
	obj.style.top= nt + 'px';
}
iSpeakVideo.prototype.CheckFlashVersion = function() {
	var maj=0;
	var min=0;
	if(navigator.plugins != null && navigator.plugins.length > 0) {
		if(navigator.plugins["Shockwave Flash"]) {
			var d=navigator.plugins["Shockwave Flash"].description;
			var a=d.split(" ");
			var version=a[2];
			var a1=version.split(".");
			maj=a1[0];
			min=a1[1];
			
		} 
	}
	else {
		try {
			// version will be set for 7.X or greater players
			var o = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
		 	var d = o.GetVariable("$version");
			var a=d.split(" ");
			var version=a[1];
			var a1=version.split(",");
			maj=a1[0];
			min=a1[1];
		} catch (e) {
		}
	}
	if(maj>this.RequiredVersionMajor || (maj==this.RequiredVersionMajor && min>=this.RequiredVersionMinor)) { return true;}
	else {	return false;}
	
}
	

iSpeakVideo.prototype.GetObjectPosition=function(o) {
	var l,t;
	l=t=0;
	if(o.offsetParent) {
		l=o.offsetLeft;
		t=o.offsetTop;
		while((o=o.offsetParent)) {
			l+=o.offsetLeft;
			t+=o.offsetTop;
		}
	}
	return [l,t];
}

iSpeakVideo.prototype.HideWhenFinished=function(){
	this.el('iSpeakVideo_Container').style.display='none';
}

iSpeakVideo.prototype.GetObjectAbsoluteLocation=function(o) {
	var p=new this.Point(0,0);
	while(o!=null) {
		p.X+=o.offsetLeft;
		p.Y+=o.offsetTop;
		o=o.offsetParent
	}
	return p;
}

iSpeakVideo.prototype.GetObjectAbsoluteX=function(o) {
	var x;
	x=0;
	while(o!=null) {
		x+=o.offsetLeft;
		o=o.offsetParent
	}
	return x;
}

iSpeakVideo.prototype.GetObjectAbsoluteY=function(o) {
	var y;
	y=0;
	while(o!=null) {
		y+=o.offsetTop;
		o=o.offsetParent
	}
	return y;
}

iSpeakVideo.prototype.WriteStandardContainer= function(){
	document.write('<div id="iSpeakVideo_Container" style="z-index:10001;');
	this.WritePositionStyleInfo();
	document.write('z-index: 10000;border: 0px; width:'+this.width+'px;height:'+this.height+';">');
}

iSpeakVideo.prototype.WritePositionStyleInfo=function() {
		
	document.write('position: '+this.positionMode+'; ');
	if(this.positionFrom=='fromBottom')
	{
		document.write('bottom: ');
	}
	else {document.write('top: ');}
	document.write(this.Location.Y+'px; ');
	if(this.positionHorizontalFrom=='fromLeft')
	{
		document.write('left: ');
	}
	else
	{
		document.write('right: ');
	}

	document.write(
	this.InitialX+'px;'); 
//	this.Location.X
}
iSpeakVideo.prototype.WriteNeedFlashContainer= function(){
	document.write('<div id="iSpeakVideo_Container" style="');
	this.WritePositionStyleInfo();
	document.write('z-index: 10000;border: 0px; width:'+this.width+'px; background-color:#FFF; padding:4px 4px 4px 4px; border:inset 3px #F00 ">');
}

iSpeakVideo.prototype.WriteDiv=function() {
	if(this.CheckFlashVersion()) {
		if(this.WriteWrapper) {
		
			this.WriteStandardContainer();
		}
		this.WriteMovieObject();
	}
	else {
		this.WriteNeedFlashContainer();
		this.WriteNeedFlash();
	}
	if(this.NeedsFix){
		this.FixIEFixed();
	}
	if(this.WriteWrapper) {
		document.write('</div>');
	}
}
iSpeakVideo.prototype.WriteNeedFlash=function() {
		document.write(this.needFlashString);
}

iSpeakVideo.prototype.LockInitialX=function() {
	var src=this.el(iSpeakVideo_Global.ReferenceID);
	var srcLoc=this.GetObjectAbsoluteX(src);
	this.InitialX=srcLoc+this.Location.X;
	}
	
function AnchorResizedX() {
	var obj=iSpeakVideo_Global.el('iSpeakVideo_Container');
	var src=iSpeakVideo_Global.el(iSpeakVideo_Global.ReferenceID);
	var srcLoc=iSpeakVideo_Global.GetObjectAbsoluteX(src);
	var nl=video.Location.X+srcLoc;
	obj.style.left=nl + 'px';
}

var shown=0;
var lastNum=0;
function getIEFixHeight() {
	var loc;
	var scrollTop;
	var clientHeight;
  if( typeof( window.innerWidth ) == 'number' ) { //Non-IE
    clientHeight = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) { //IE 6+ in 'standards compliant mode'
    clientHeight  = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) { //IE 4 compatible
    clientHeight  = document.body.clientHeight;
  }
	if( typeof( window.pageYOffset ) == 'number' ) {	//Netscape compliant
    scrollTop = window.pageYOffset;
	} else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) { //DOM compliant
    scrollTop = document.body.scrollTop;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
    scrollTop = document.documentElement.scrollTop;
  } else {scrollTop=0;}
	if(iSpeakVideo_Global.positionFrom=='fromBottom'){
		loc=scrollTop+clientHeight-iSpeakVideo_Global.Location.Y-iSpeakVideo_Global.el('iSpeakVideo_Container').clientHeight ;	
	} else {
		loc=scrollTop+iSpeakVideo_Global.Location.Y;
	}
	//if(shown<2 && lastNum!=loc) 
	//	{ alert(loc);shown++;lastNum=loc;}	
	return loc;
}

iSpeakVideo.prototype.WriteMovieObject=function()
{
	document.write('<object ');
	if(!this.WriteWrapper) { 
		document.write(' id="iSpeakVideo_Container"  style="');
		this.WritePositionStyleInfo();
		document.write('z-index: 1000;"');
	} else {
		document.write(' id="iSpeakVideo_Container_o"  ');
	}
	document.write(' classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,19,0" width="');
	document.write(this.width+'px" height="'+this.height+'">\n');
	document.write('\t<param name="movie" value="'+this.src+'" />\n');
	document.write('\t<param name="quality" value="high" />\n');
	if(this.FlashVars!='') 
	{
		document.write('\t<param name="FlashVars" value="'+this.FlashVars+'"');
	}
	document.write('\t<param name="menu" value="false" />\n');
	document.write('\t<param name="wmode" value="transparent" />\n');
	document.write('\t<embed id="iSpeakVideoVideo" src="'+this.src+'" quality="high" wmode="transparent" pluginspage="https://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="');
	document.write(this.width+'px" height="'+this.height+'" menu="false"');
	if(this.FlashVars!='') 
	{
		document.write('FlashVars="'+this.FlashVars+'"');
	}
	document.write('></embed>\n');
	document.write('</object>\n');
}

iSpeakVideo.prototype.FixIEFixed=function(){
	if(document.body.currentStyle['backgroundImage']=='none')
	{
		document.body.style.backgroundImage='url('+this.backgroundImage+')';
		document.body.style.backgroundRepeat='no-repeat';
		document.body.style.backgroundAttachment='fixed';
	}
	this.el('iSpeakVideo_Container').style.setExpression('top','getIEFixHeight()');
}

iSpeakVideo.prototype.FixFF15Position=function(){
	var scrollTop=	window.pageYOffset;
	var clientHeight=window.innerHeight;

	if(this.InitialHeight== undefined) { this.InitialHeight=this.el('iSpeakVideo_Container').clientHeight;}
	if(this.positionFrom=='fromBottom'){
		loc = scrollTop + clientHeight - 
				this.Location.Y-this.InitialHeight ;	
	} else {
		loc=scrollTop+iSpeakVideo_Global.Location.Y;
	}
	this.el('iSpeakVideo_Container').style.top=loc+'px';
}

function RestorePosition() {
	iSpeakVideo_Global.FixFF15Position();
}
