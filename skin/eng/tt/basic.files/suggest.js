// Specify the time between keypresses required before the database is searched for suggestions.
// I recommend a value between 350 and 600.  A higher value results in fewer database queries and so a decreased server resource usage.
// Stores with many products (or stores on a shared webhosting plan) are advised NOT specify a value smaller than 500!

var delay = 500;

//------------------------------------------------------------------------------------------------------------//
/* 	
	
	#####################################################################################
	 ____  _                 ____             
	/ ___|| |__   ___  _ __ |  _ \  _____   __
	\___ \| '_ \ / _ \| '_ \| | | |/ _ \ \ / /
	 ___) | | | | (_) | |_) | |_| |  __/\ V / 
	|____/|_| |_|\___/| .__/|____/ \___| \_/  
    	              |_|                     			  
	#####################################################################################
  
  	MOD NAME:			AJAX Search Suggestion
  	FILE NAME:			suggest.js
  	VERSION:			1.0
  	DATE RELEASED:		19/08/07
  	COPYRIGHT:			ShopDev
  	WEBSITE:			http://www.shopdev.co.uk
  	COMPATIBILITY:		CubeCart V3.X
  	SUPPORT:			admin@shopdev.co.uk
 	UPLOAD TO:			js/
	   
	#####################################################################################  

*/	

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////   DO NOT EDIT BELOW   ///////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

//Gets the XmlHttpRequest Object - browser specific!
function getXmlHttpRequestObject() {
	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else if(window.ActiveXObject) {
		return new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		alert("Browser not supported!");
	}
}

//Our XmlHttpRequest object to get the auto suggest
var searchReq = getXmlHttpRequestObject();
	
	
	
//Set time delay and count
var count = 0;
function doDropDown()
{
	count = count+1;
	setTimeout("dropDownGo("+count+")",delay);
}
function dropDownGo(currCount)
{
	if(currCount == count)
{
count = 0;
searchSuggest();
}
} 
	  
	  

//Called from keyup event and limited by time delay.
//Start AJAX request.
function searchSuggest() {
	if (searchReq.readyState == 4 || searchReq.readyState == 0) {
		var str = escape(document.getElementById('searchStr').value);
		searchReq.open("GET", 'suggest.php?searchStr=' + str + '&act=viewCat', true);
		searchReq.onreadystatechange = handleSearchSuggest; 
		searchReq.send(null);
	}		
}

//This is called when the response is returned.
function handleSearchSuggest() {
	if (searchReq.readyState == 4) {
		var ss = document.getElementById('search_suggest')
		ss.innerHTML = '';
		var str = searchReq.responseText.split("<br />");
		for(i=0; i < str.length - 1; i++) {
			
			//Build our element string.
			var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
			suggest += 'onmouseout="javascript:suggestOut(this);" ';
			suggest += 'class="suggest_link">' + str[i] + '</div>';
			ss.innerHTML += suggest;
		}
	}
}

//Mouse over function
function suggestOver(div_value) {
	div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
	div_value.className = 'suggest_link';
}
//Click function
function setSearch(value) {
	document.getElementById('searchStr').value = value;
	document.getElementById('search_suggest').innerHTML = '';
}