
if(typeof thefind=='undefined'){thefind=new function(){this.func={};this.searches={};this.add=function(name,obj){switch(typeof obj){case'function':this.func[name]=obj;break;default:if(!this[name])this[name]=obj;break;}}
this.search=function(){for(var key in this.searches)
return this.searches[key];return this.create_search();}
this.create_search=function(name,request,options){var name=!name?'tf_search_examplesearch':name;search=this.searches[name]=new this.func.search(name,request,options);return search;}
this.find=function(selectors,parent){if(document.querySelectorAll)
return(parent)?parent.querySelectorAll(selectors):document.querySelectorAll(selectors);return thefind.findCore(selectors,parent);}
this.findCore=function(selectors,oparent){var selectors=selectors.split(','),elements=[],selector,section,tag,tags,classname,isParent,parent,parents;for(var s=0;s<selectors.length;s++){parent=oparent||document.getElementsByTagName('BODY')[0];parents=[parent];section=selectors[s].split(' ');for(var p=0;parent=parents[p];p++){for(var q=0;q<section.length;q++){isParent=(q=section.length-1);selector=section[q].split('.');tag=selector[0]||'*';tags=parent.getElementsByTagName(tag);classname=selector.length>1?selector[1]:false;for(var i=0;i<tags.length;i++){if(classname){if(thefind.utils.hasClass(tags[i],classname))
if(isParent)
parents.push(tags[i]);else
elements.push(tags[i]);}else
if(isParent)
parents.push(tags[i]);else
elements.push(tags[i]);}}}}
return elements;}
this.checkHash=setInterval(function(){if(typeof thefind.ajax_back_button=='object')
thefind.ajax_back_button.check();},500);this.fetch=function(url,callback){ajaxlib.Queue({url:url,callback:[this,callback]});}}}
if(!window.console){window.console={};window.console.log=function(txt){thefind.func.log(txt);}}else{window.console.log=function(txt){if(typeof(thefind.debug)!='undefined')
thefind.func.log(txt);if(console&&typeof console.debug!='undefined')
console.debug.apply(this,arguments);}}
thefind.add('ga',function(selector,obj,event){var event=event||'click';$TF(selector)[event](function(){if(typeof googleAnalytics!='undefined'){googleAnalytics.trackEvent(obj);}});});thefind.add('log',function(txt){if(typeof thefind.debug!='undefined')
thefind.debug.log(txt);else{if(!thefind.prelog)
thefind.prelog=[];thefind.prelog.push(txt);}});thefind.add('getActiveItem',function(whole_item){var results=search.getBindings('results','main'),results=results.length>0?results[0]:false;if(results)
var index=results.getItemNumber(results.current),item=index?results.items[index]:false;return item?whole_item?item:arrayGet(item,'item'):false;});thefind.add('infobox_stores',function(){$TF.get("/facebook/stores_match.html",function(html){thefind.lightbox.show(html);});});thefind.add('tell_more_friends',function(){var callback=window.location.href;return thefind.lightbox.get("/facebook/tell_more_friends.html","callback="+encodeURIComponent(callback));});thefind.add('infobox_privacy_settings',function(){return thefind.lightbox.get("/user/privacy_settings.html");});thefind.add('twitter_form',function(){var form=document.getElementById('tf_share_twitter'),item=thefind.func.getActiveItem(),infobox=thefind.infobox.get('product_infocard'),href=window.location.href.split('#')[0],query=thefind.searches.tf_search_examplesearch.args.query,shortHREF='';if(query){var message="\n\nI've searched for "+query+" on @TheFind. Look at these great products I found!";}else{var message="\n\nTake a look at these great results at TheFind.com";}
if(item&&infobox&&infobox.visible){href+='&ddkey='+item.ddkey;}
function setMessage(args){if(shortHREF){href=shortHREF;}
if(item&&infobox&&infobox.visible){message="I'm looking at "+item.title+", "+href+" on @TheFind.";form.msg.innerHTML=message;}
else{form.msg.innerHTML=href+message;}}
$TF.ajax({url:'/utils/shorturl.js',data:'url='+encodeURIComponent(href),dataType:'json',type:'GET',timeout:5000,success:function(data,textStatus){shortHREF=data.data.shorturl;setMessage();},error:function(XMLHttpRequest,textStatus,errorThrown){setMessage();}});});thefind.add('email_form',function(args){var args=args||{},data=thefind.user.user,to=document.getElementById('myfindsSendEmailToEmail'),from=document.getElementById('myfindsSendEmailFromEmail'),name=document.getElementById('myfindsSendEmailFromName'),msg=document.getElementById('myfindsSendEmailMessage'),url=window.location.href.split('#')[0]
sep=url.split('?').length>1?'&':'?';if(from&&data.email)
from.value=data.email;if(name&&data.nickname)
name.value=data.nickname;if(msg)
if(arrayGet(args,"isproduct")){msg.value="I just discovered this product on TheFind and wanted to share it with you.\n\n"+url+sep+"ddkey="+arrayGet(args,'ddkey')+"\n\n";}
else{msg.value="I just discovered these products on TheFind and wanted to share them with you.\n\n"+url+"\n\nCheck them out!";}
if(to)
to.focus();});thefind.add('add_mystores_filter',function(stores){thefind.mystores_hack_data=stores;var filters=thefind.search().getBindings('filters','store');for(var i=0;i<filters.length;i++){store=filters[i];store.init_mystores();}});thefind.add('add_results',function(parms){if(parms.extra)
for(var key in parms.extra)
parms.args[key]=parms.extra[key];if(parms.type!='none'&&parms.args)
parms.args.placement=parms.type;thefind.search().Bind("results",parms.type,parms.args,parms.id,parms.data,parms.relatedchildren);});thefind.add('add_info',function(parms){if(!parms)
return;parms.args.ajax=parms.ajax;parms.args.id='tf_search_item_actions_savesearchinmyfinds_link';thefind.search().Bind("info",parms.placement,parms.args);});thefind.add('add_input',function(parms){parms.args.ajax=parms.ajax;thefind.search().Bind("input",parms.placement,parms.args);});thefind.add('add_list',function(parms){if(parms.name=='brand'||parms.name=='store')
for(var key in thefind.search().bindings['filters'])
for(var jskey in thefind.search().bindings['filters'][key]['filters']){var filter=thefind.search().bindings['filters'][key]['filters'][jskey];if(jskey==parms.name&&typeof filter.checked=='object'){for(var i=0;i<filter.checked.length;i++){element=document.getElementById('tf_search_filters_'+parms.placement+'_'+parms.type+'_'+parms.name+'_'+filter.checked[i]);if(element)
element.checked='checked';}
filter.checked=[];thefind.search().SearchParms['filter['+parms.name+']']=[];}}});thefind.add('add_map',function(parms){thefind.search().SearchParms.local=1;var inc=0;this.init=function(parms){try{this.run(parms);}catch(e){this.timer();}}
this.run=function(parms){$TF(document).ready(function(){if(++inc>15)
return false;this.local=new thefind.func.local(search,{idprefix:"tf_local",location:parms.location,sites:parms.sites,settings:parms.settings});thefind.local=this.local;thefind.local_num=thefind.local_num?thefind.local_num+1:1;this.local.InitMap("tf_local_map");if(parms.change_loc){var button,buttons=$TF('.tf_search_filters_location_button');for(var i=0;i<buttons.length;i++){button=buttons[i];thefind.infobox.add('localsettings_popup'+thefind.local_num,{margin:10,absolute:true,width:'22em',event:'click',fixed:'tf_search_filters_right',border:'page.tfinfobox',titlebar:true,nocache:true,ajax:1},"&placement="+parms.placement,'/local/settings',button,true);}}});}
this.timer=function(){(function(self){setTimeout(function(){self.init(parms);},200);})(this);}
this.init(parms);});thefind.add('add_location',function(location){thefind.location={city:location[0],state:location[1],type:(location[2]?location[2]:(location[0]?'ip_geolocation':'none'))};$TF(document).ready(function(){var local_checkbox=document.getElementById('tf_search_filters_toggle_localtoggle_input'),local_tabs=document.getElementById('tf_search_filters_list_store_tabs');if(local_tabs)
local_tabs=local_tabs.getElementsByTagName('li');if(local_checkbox){local_checkbox.disabled=!location[0];if(local_checkbox.checked&&!location[0])
local_checkbox.checked=false;}
if(arrayGet(local_tabs,'length')>1)
local_tabs[1].style.display=(location[0]?'block':'none');});});thefind.add('myfinds_panel_submit',function(form,type){var parms='';switch(type){case'brands':type='brand';break;case'stores':type='store';break;}
$TF("input:checkbox:checked",form).each(function(k,v){if(v.value)
parms+=(parms==''?'':',')+v.value;});var u=window.location.href.split('?'),n=u[0]+'?'+type+'='+parms;window.location.href=n;return false;});thefind.add('change_location',function(form){this.ajax=function(address){this.close();thefind.func.ajax_submit({parms:{local:1,location:address}});return;}
this.address=function(){for(var inputs=form.getElementsByTagName('input'),i=0;i<inputs.length;i++)
if(inputs[i].name=='location'||inputs[i].name=='localsettings[location]')
if(inputs[i].value)
return true;return;}
this.close=function(){setTimeout(function(){thefind.infobox.hide("localsettings_popup");for(var i=1;i<=thefind.local_num;i++)
thefind.infobox.hide("localsettings_popup"+i);},750);}
if(thefind.local){thefind.local.ClearMarkers();return window.location.reload();}else if(typeof thefind.search().bindings.results!='undefined'){this.close();window.location.reload();return this.address();}else{this.close();window.location.reload();return;}});thefind.add('getMouseXY',function(event){if(typeof event.touches!='undefined'&&event.touches.length>0)
var x=event.touches[0].pageX,y=event.touches[0].pageY;else
var x=event.pageX||(event.clientX+document.body.scrollLeft),y=event.pageY||(event.clientY+document.body.scrollTop);return{'x':x,'y':y};});thefind.add('googleAnalytics_myfinds',function(type,section,formtag){if(typeof googleAnalytics!='object')
return false;$TF("div#tf_myfinds_saved_"+type+" a.tf_search_item_link strong").click(function(){googleAnalytics.clickoutsource=(section=='saved')?8:11;});$TF("div#tf_myfinds_saved_{$type} a.tf_search_item_link").click(function(){if(!googleAnalytics.clickoutsource)
googleAnalytics.clickoutsource=(section=='saved')?9:12;googleAnalytics.myfindspanel='myfavs_'+section;});$TF("a.tf_myfinds_saved_searches_link").each(function(n){$TF(this).click(function(){googleAnalytics.trackEvent(['links',googleAnalytics.pagetype,'recent_searches',n+1])});});if(formtag)
$TF("#tf_myfinds_searchform_"+type).submit(function(){var query=document.getElementById('tf_myfinds_searchform_'+type).query.value;if(query)googleAnalytics.trackEvent(['search','myfavorites.'+type,query]);});});thefind.add('bezier_curve',function(){this.order=2;this.cp=[];this.curve=[];this.points=function(points){this.order=points.length-1;for(var i=0;i<points.length;i++)
this.cp[i]=points[i];}
this.bf=function(i,t){var o=this.order,h=this.h||(this.h=o/2),f=this.f||(this.f=parseInt(h)),c=this.c||(this.c=(h==f)?f:f+1),x=o*(f-(i<=f?f-i:i-c))||1,a=Math.pow(t,o-i).toFixed(8)||1,b=Math.pow(1-t,i).toFixed(8)||1;return x*a*b;}
this.calc=function(precision,noround){for(var i=0;i<=precision;i++)
this.curve[i]=(noround)?this.pos(i/precision):Math.round(this.pos(i/precision));return this.curve;}
this.pos=function(p){for(var pos=i=0;i<=this.order;i++)
pos+=this.cp[i]*this.bf(i,p);return pos;}
this.draw=function(color,debug,invert,container,array){if(!this.container)
this.container=container||$TF('#tf_container')[0];array=array||this.curve;invert=(invert)?100:0;if(debug)
console.log(array);for(var i=0;i<this.curve.length;i++){var point=document.createElement('div');this.container.appendChild(point);$TF(point).css({position:'absolute',background:color,width:2+'px',height:2+'px',left:i+'px',top:invert-array[i]+'px'});}}});thefind.add('create',function(parms,classname,style,additional,append,before){if(typeof parms=='object')
var tag=parms.tag,classname=parms.classname,style=parms.style,additional=parms.attributes,append=parms.append,before=parms.before;var element=document.createElement(tag||parms);if(classname)
element.className=classname;if(style)
for(var property in style)
element.style[property]=style[property];if(additional)
for(var property in additional)
element[property]=additional[property];if(append)
if(before)
append.insertBefore(element,before);else
append.appendChild(element);return element;});thefind.add('bind',function(elements,types,fn){if(!elements||!types||!fn||typeof types!="string")
return;var elements=(!thefind.utils.isNull(elements.nodeName)||elements==window)?[elements]:elements,types=types.split(',');for(var e=0;e<elements.length;e++){var obj=elements[e];if(typeof obj!='object')
continue;for(var i=0;i<types.length;i++){var type=types[i];if("addEventListener"in obj){if(type=='mousewheel'&&thefind.browser.type!='safari')
type='DOMMouseScroll';if(typeof fn=="object"&&fn.handleEvent){obj[type+fn]=function(e){fn.handleEvent(e);}
obj.addEventListener(type,obj[type+fn],false);}else{obj.addEventListener(type,fn,false);}}else if(obj.attachEvent){if(typeof fn=="object"&&fn.handleEvent){obj[type+fn]=function(){fn.handleEvent(thefind.utils.fixEvent(window.event));}}else{obj["e"+type+fn]=fn;obj[type+fn]=function(){if(typeof obj["e"+type+fn]=='function')
obj["e"+type+fn](thefind.utils.fixEvent(window.event));}}
obj.attachEvent("on"+type,obj[type+fn]);}}}
return this;});thefind.add('unbind',function(elements,types,fn){if(!elements||!types||!fn||typeof types!="string")
return;var elements=(!thefind.utils.isNull(elements.nodeName)||elements==window)?[elements]:elements,types=types.split(',');for(var e=0;e<elements.length;e++){var obj=elements[e];if(typeof obj!='object')
continue;for(var i=0;i<types.length;i++){var type=types[i];if(obj.removeEventListener){if(typeof fn=="object"&&fn.handleEvent){obj.removeEventListener(type,obj[type+fn],false);delete obj[type+fn];}else{obj.removeEventListener(type,fn,false);}}else if(obj.detachEvent){if(typeof obj[type+fn]=="function")
obj.detachEvent("on"+type,obj[type+fn]);obj[type+fn]=null;obj["e"+type+fn]=null;}}}
return this;});thefind.add('get_scroll',function(shpadoinkle){if(thefind.iphone&&thefind.iphone.scrollcontent)
var pos=[0,0];else if(typeof pageYOffset!='undefined')
var pos=[pageXOffset,pageYOffset];else
var QuirksObj=document.body,DoctypeObj=document.documentElement,element=(DoctypeObj.clientHeight)?DoctypeObj:QuirksObj,pos=[element.scrollLeft,element.scrollTop];switch(shpadoinkle){case 0:return pos[0];case 1:return pos[1];default:return[pos[0],pos[1]];}});thefind.add("bucketize",function(array,num){var result=[];for(var i=0;i<array.length;i++){var value=array[i],bucket=Math.floor(i/num);if(typeof result[bucket]!='object')
result[bucket]=[];result[bucket].push(value);}
return result;});thefind.add('dimensions',function(element,ignore_size){if(typeof element!='object'||element===window){var width=window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth,height=window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight;return{0:width,1:height,x:0,y:0,w:width,h:height,s:thefind.func.get_scroll()};}
var width=ignore_size?0:element.offsetWidth,height=ignore_size?0:element.offsetHeight,left=element.offsetLeft,top=element.offsetTop,id=element.id||'';while(element=element.offsetParent){top+=element.offsetTop-element.scrollTop;left+=element.offsetLeft-element.scrollLeft;}
if(thefind.browser.type=='safari')
top+=thefind.func.get_scroll(1);return{0:left,1:top,x:left,y:top,w:width,h:height};});thefind.add('browser',new function(){this.checkIt=function(string){this.place=detect.indexOf(string)+1;this.tmpstring=string;return this.place;}
var detect=navigator.userAgent.toLowerCase();if(this.checkIt('konqueror')){this.type="Konqueror";this.OS="Linux";}
else if(this.checkIt('safari'))this.type="safari";else if(this.checkIt('omniweb'))this.type="omniweb";else if(this.checkIt('opera'))this.type="opera";else if(this.checkIt('webtv'))this.type="webtv";else if(this.checkIt('icab'))this.type="icab";else if(this.checkIt('msie'))this.type="msie";else if(!this.checkIt('compatible')){this.type="netscape";this.version=detect.charAt(8);}
else this.type="unknown";if(!this.version)this.version=detect.charAt(this.place+this.tmpstring.length);if(!this.OS){if(this.checkIt('linux'))this.OS="linux";else if(this.checkIt('x11'))this.OS="unix";else if(this.checkIt('mac'))this.OS="mac";else if(this.checkIt('win'))this.OS="windows";else this.OS="unknown";}
if(this.type+this.version=='msie6')
thefind.ie6=true;});thefind.add('file_utils',new function(){this.get=function(type,file,func){if(!type||!file)
return false;var head=document.getElementsByTagName("HEAD")[0],element=document.createElement((type=='javascript'?"SCRIPT":"LINK"));if(type=='javascript'){element.type="text/javascript";element.src=file;}else{element.type="text/css";element.rel="stylesheet";element.href=file;}
if(func)
element.onload=func;head.appendChild(element);return element;}});thefind.add('dependencies_batch',function(){this.callbacks=[];this.files=[];this.add=function(url,type,component){if(typeof url=='string'){var dep=thefind.dependencies.add(url,this,type,component)
if(dep)
this.files.push(dep);}}
this.callback=function(script){this.callbacks.push(script);if(this.files.length==0)
this.done(true);}
this.done=function(url){if(url)
for(var i=0;i<this.files.length;i++)
if(!this.files[i].loaded&&this.files[i].type!='css')
return;for(var i=0;i<this.callbacks.length;i++)
switch(typeof this.callbacks[i]){case"string":eval(this.callbacks[i]);break;case"function":this.callbacks[i]();break;}
this.callbacks=[];}});thefind.add('dependencies',new function(){this.host='';this.files={};this.registered={javascript:{},css:{}};this.waiting={javascript:{},css:{}};this.register=function(sFile,check,type){var type=type||'javascript',r=this.registered[type],w=this.waiting[type];if(r[sFile])
return;if(typeof check=='undefined')
check=true;r[sFile]=true;if(w[sFile]){var url=w[sFile],file=this.files[url],components=this.getComponents(url);delete w[sFile];this.checkWaiting(file,components,type);}}
this.registerMany=function(components,type){for(var k in components)
if(components.hasOwnProperty(k)&&components[k].length>0)
for(var i=0;i<components[k].length;i++)
if(components[k][i]!=null)
this.register(k+'.'+components[k][i],false,type);}
this.checkWaiting=function(file,components,type){var type=type||'javascript',w=this.waiting[type],b=true;for(var i=0;i<components.length;i++){if(w[components[i]]){b=false;break;}}
if(b)
this.done(file);}
this.getComponents=function(url){var ret=[],url=url.split('?'),page=url[0],parms=url[1].split('&');for(var i=0;i<parms.length;i++){var parm=parms[i].split('='),files=parm[1].split('+');for(var f=0;f<files.length;f++){file=parm[0]+'.'+files[f];ret.push(file);}}
return ret;}
this.wait=function(url,type){var type=type||'javascript',r=this.registered[type],w=this.waiting[type],components=this.getComponents(url);for(var i=0;i<components.length;i++)
if(!r[components[i]])
w[components[i]]=true;url=this.url(w);for(var key in w)
w[key]='/'+(type=='css'?'css':'scripts')+'/main'+url;return url;}
this.url=function(oParms){var parms={},ret='';for(var key in oParms){parm=key.split('.');if(!parms[parm[0]])
parms[parm[0]]=[];parms[parm[0]].push(parm[1]);}
for(var key in parms){ret+=(ret==''?'?':'&')+key+'=';for(var i=0;i<parms[key].length;i++){if(parms[key][i]!='map')
ret+=parms[key][i]+(i==parms[key].length-1?'':'+');else if(i==parms[key].length-1)
ret=ret.substr(0,ret.length-1);}}
if(ret.indexOf("=")<0)
ret='';return ret;}
this.done=function(oFile){if(typeof oFile!='undefined'){oFile.loaded=true;if(oFile.batch)
oFile.batch.done(oFile.url);}}
this.add=function(url,batch,type,component){var file=this.files[url]||{},type=type||'javascript';if(!thefind.utils.isNull(file.url)){if(batch){batch.done(url);return file;}}
if(component||type=='css'){url=this.wait(url,type);if(url)
url='/'+(type=='css'?'css':'scripts')+'/main'+url;else
return false;}
file.batch=batch;file.loaded=false;file.url=url;file.type=type;file.element=thefind.file_utils.get(type,this.host+url,((component)?null:(function(self){self.done(file);})(this)));this.files[url]=file;return file;}});thefind.add('component',new function(){this.ids={};this.host='';this.get=function(id,name,args){if(name){var url=this.host+'/'+name.replace("\.","/")+".js";if(args){if(typeof args=='object'){var urlsep="?";for(var k in args){if(k&&args[k])
url+=urlsep+k+"="+args[k];urlsep="&";}}else if(typeof args=='string'){url+="?"+args;}}
thefind.file_utils.get("javascript",url);}}
this.response=function(reqid,contents){if(reqid&&this.ids[reqid]){var id=this.ids[reqid];$TF("#"+id).html(contents);}}
this.generateRequestID=function(){return(parseInt(new Date().getTime().toString().substring(0,10))+parseFloat(Math.random()));}});thefind.add('onloads',new function(){this.done=false;this.onloads=[];this.add=function(expr){this.onloads.push(expr);}
this.init=function(){if(/WebKit/i.test(navigator.userAgent)){this.timer=setInterval(function(){if(/loaded|complete/.test(document.readyState)){thefind.onloads.execute();}},10);return;}
if(document.addEventListener){document.addEventListener("DOMContentLoaded",thefind.onloads.execute,false);return;}
window.onload=thefind.onloads.execute;}
this.execute=function(){if(thefind.onloads.done)return;thefind.onloads.done=true;if(thefind.onloads.timer)clearInterval(thefind.onloads.timer);var script='';var expr;while(expr=thefind.onloads.onloads.shift()){if(typeof expr=='function'){expr();}else{script+=expr+(expr.charAt(expr.length-1)!=';'?';':'');}}
eval(script);}});thefind.add('log_size',function(result_view_id){if(typeof result_view_id=='undefined')
result_view_id='';if(window.innerWidth)
var tr_width=window.innerWidth,tr_height=window.innerHeight;else
if(document.body.offsetWidth)
var tr_width=document.body.offsetWidth,tr_height=document.body.offsetHeight;if(ajaxlib){ajaxlib.Get('/page/sizelog?width='+tr_width+'&height='+tr_height+'&result_view_id='+result_view_id);}});thefind.add("utils",new function(){this.encodemap={"_":"//","/":"_","+":"&&","&":"+","-":"~"," ":"-","\"":"%22","'":"%27"};this.regexps={};this.FriendlyURLEncode=function(str){var ret=str;var utils=this;if(typeof str=='string')
$TF.each(this.encodemap,function(key,val){if(!utils.regexps[key]){utils.regexps[key]=new RegExp(utils.escapeRegexp(key),"g");}ret=ret.replace(utils.regexps[key],val);});else if(typeof str=='array')
ret=str.join(",");return ret;}
this.escapeRegexp=function(text){if(!this.regexps.regexp){var specials=['/','.','*','+','?','|','(',')','[',']','{','}','\\'];this.regexps.regexp=new RegExp('(\\'+specials.join('|\\')+')','g');}
return text.replace(this.regexps.regexp,'\\$1');}
this.encodeURLParams=function(obj){var value,ret='';if(typeof obj=="string"){ret=obj;}else{for(var key in obj){ret+=(ret!=''?'&':'')+key+'='+encodeURIComponent(obj[key]);}}
return ret;}
this.parseURL=function(str){var ret={uri:str,args:{}};var parts=str.split("?");if(parts[0]){var fileparts=parts[0].split(/:\/\//,2);if(fileparts[1]){ret.scheme=fileparts[0];if(fileparts[1][0]=='/'){ret.host=document.location.host;ret.path=fileparts[1];}else{var pathparts=fileparts[1].split("/");ret.host=pathparts.shift();ret.path='/'+pathparts.join("/");}}else{ret.scheme=document.location.protocol.slice(0,-1);ret.host=document.location.host;ret.path=fileparts[0];}}
if(parts[1]){var args=parts[1].split("&");ret.args={};for(var i=0;i<args.length;i++){var argparts=args[i].split("=",2);ret.args[argparts[0]]=decodeURIComponent(argparts[1]);}}
return ret;}
this.makeURL=function(obj){var argstr=thefind.utils.encodeURLParams(obj.args);return obj.scheme+"://"+obj.host+obj.path+(argstr?'?'+argstr:'');}
this.getElementsByClassName=function(oElm,strTagName,strClassName){var arrElements=(strTagName=="*"&&oElm.all)?oElm.all:oElm.getElementsByTagName(strTagName),arrReturnElements=new Array(),oElement;strClassName=strClassName.replace(/\-/g,"\\-");var oRegExp=new RegExp("(^|\\s)"+strClassName+"(\\s|$)");for(var i=0;i<arrElements.length;i++){oElement=arrElements[i];if(oRegExp.test(oElement.className)){arrReturnElements.push(oElement);}}
return arrReturnElements;}
this.isTrue=function(obj){if(obj==true||obj=='true')
return true;return false;}
this.isNull=function(obj){if(obj==null||typeof obj=='undefined')
return true;return false;}
this.isEmpty=function(obj){if(obj!==null&&obj!==""&&obj!==0&&typeof obj!=="undefined"&&obj!==false)
return false;return true;}
this.elementAddClass=function(element,className){if(this.hasClass(element,className))
return;if(element.className.length>0)
element.className+=" "+className;else
element.className=className;}
this.elementRemoveClass=function(element,className){var re=new RegExp("(^| )"+className+"( |$)","g");if(this.hasClass(element,className))
element.className=element.className.replace(re," ");}
this.elementHasClass=function(element,className){if(element&&element.className){var re=new RegExp("(^| )"+className+"( |$)","g");return(element.className.match(re)!=null);}
return false;}
this.addClass=this.elementAddClass;this.removeClass=this.elementRemoveClass;this.hasClass=this.elementHasClass;this.toggleClass=function(element,className){if(this.hasClass(element,className))
this.removeClass(element,className)
else
this.addClass(element,className);}
this.getFirstChild=function(obj,tag,className){for(var i=0;i<obj.childNodes.length;i++)
if(obj.childNodes[i].nodeName==tag.toUpperCase())
if(className&&this.hasClass(obj,className))
return obj.childNodes[i];else if(!className)
return obj.childNodes[i];return null;}
this.getLastChild=function(obj,tag,className){for(var i=obj.childNodes.length-1;i>=0;i--)
if(obj.childNodes[i].nodeName==tag.toUpperCase())
if(className&&this.hasClass(obj,className))
return obj.childNodes[i];else if(!className)
return obj.childNodes[i];return null;}
this.getAll=function(obj,tag,className){var ret=[],all=obj.getElementsByTagName(tag);for(var i=0;i<all.length;i++)
if(className&&this.hasClass(all[i],className))
ret.push(all[i]);else if(!className)
ret.push(all[i]);return ret;}
this.getOnly=function(obj,tag,className){if(!obj||!tag)
return;var ret=[];for(var i=0;el=obj.childNodes[i];i++)
if(el.nodeName==tag.toUpperCase()){if(className&&this.hasClass(el,className))
ret.push(el);else if(!className)
ret.push(el);}
return ret;}
this.getParent=function(element,tag,all_occurrences){var ret=[];while(element&&element.nodeName!='BODY'){if(element.nodeName==tag.toUpperCase()){if(all_occurrences)
ret.push(element);else
return element;}
element=element.parentNode;}
return(ret.length==0?false:ret);}
this.find=thefind.find;this.getTarget=function(event){return window.event?event.srcElement:event.target;}
this.getRelated=this.getRelatedTarget;this.getRelatedTarget=function(event){var reltg;if(event.relatedTarget){reltg=event.relatedTarget;}else{if(event.type=="mouseover")
reltg=event.fromElement;else if(event.type=="mouseout")
reltg=event.toElement;else
reltg=document;}
return reltg;}
this.getEventTarget=function(event,parentClassName){var target;if(!event)
var event=window.event;if(event.target)
target=event.target;else if(event.srcElement)
target=event.srcElement;if(target.nodeType==3)
target=target.parentNode;if(parentClassName){var classUp,classDown;if(parentClassName.indexOf(">")){var classes=parentClassName.split(">",2);classDown=classes[0];classUp=classes[1];}else{classDown=parentClassName;}
while(!this.elementHasClass(target,classDown)&&target.parentNode){target=target.parentNode;}
if(classUp){var elements;elements=this.getElementsByClassName(target,"*",classUp);if(elements.length>0){target=elements[0];}}}
return target;}
this.eventIsTransition=function(event,parent){var tg=this.getTarget(event),reltg=this.getRelatedTarget(event);return(this.elementIsIn(tg,parent)&&!this.elementIsIn(reltg,parent));}
this.fixEvent=function(event){this.preventDefault=function(){this.returnValue=false;}
this.stopPropagation=function(){this.cancelBubble=true;}
event.preventDefault=this.preventDefault;event.stopPropagation=this.stopPropagation;return event;}
this.arrayGet=function(obj,element){var ptr=obj;var x=element.split(".");for(var i=0;i<x.length;i++){if(ptr==null||(typeof ptr[x[i]]!='array'&&typeof ptr[x[i]]!='object'&&i!=x.length-1)){ptr=null;break;}
ptr=ptr[x[i]];}
return(typeof ptr=="undefined"?null:ptr);}
this.arraySet=function(obj,element,value){var ptr=obj;var x=element.split(".");for(var i=0;i<x.length-1;i++){if(ptr==null||(typeof ptr[x[i]]!='array'&&typeof ptr[x[i]]!='object'&&i!=x.length-1)){ptr[x[i]]={};}
ptr=ptr[x[i]];}
if(typeof ptr=="object"){ptr[x[x.length-1]]=value;}}
this.getEventXY=function(ev){var x,y;if(ev.touches){x=ev.touches[0].clientX;y=ev.touches[0].clientY;}else{x=ev.clientX;y=ev.clientY;}
return[x,y];}});thefind.add('set_location',function(city,state){var link=$TF('a.tf_search_filters_picker_link')[0],type=arrayGet(thefind,'location.type'),override=["none","ip_geolocation"];if(type&&indexOf(override,type)>=0)
link.innerHTML=city+', '+state;});thefind.add('hover',function(){this.init=function(element,mouseover,mouseout,alternate,click){if(!element||!mouseover||!mouseout)
return;this.element=element;this.mouseover=mouseover;this.mouseout=mouseout;this.click=click;this.alternate=alternate||element;thefind.func.bind(element,"mouseover",this);thefind.func.bind(element,"mouseout",this);if(click)
thefind.func.bind(this.alternate,"click",this);}
this.handleEvent=function(event){var event=this.event=event||window.event,target=this.target=event.target||event.srcElement,related=this.related=thefind.utils.getRelatedTarget(event);if(this.checkRelated(target,related))
return;switch(event.type){case"mouseover":this.mouseover();break;case"mouseout":this.mouseout();break;case"click":this.click();break;}}
this.checkRelated=function(target,related){while(!thefind.utils.isNull(related)){if(related==this.element)
return true;related=related.parentNode;}
return false;}});if(!Array.indexOf){Array.prototype.indexOf=function(obj){for(var i=0;i<this.length;i++)
if(this[i]==obj)
return i;return-1;}}
thefind.add('stringify',function(parms){var value,ret='';for(var key in parms){value=parms[key];ret+=key+'='+value+'&';}
return ret.substr(0,ret.length-1);});thefind.add('data',new function(){this.add=function(name,data){if(!this[name])
this[name]=[];for(var i=0;i<data.length;i++)
this[name].push(data[i]);}
this.find=function(name,path,value){if(thefind.utils.isNull(this[name]))
return false;for(var i=0;i<this[name].length;i++){var item=this[name][i],property=arrayGet(item,path);if(property==value)
return item;}
return false;}});thefind.add('combobox',function(parent,callback){this.visible=false;this.parent=parent;this.callback=callback;this.init=function(){var selects=thefind.find("select.tf_search_input_sub_navigation",this.parent),select,dim,combobox,label,button,ul,lis,img,option,actions,options;for(var i=0;i<selects.length;i++){select=selects[i];options=[];combobox=this.combobox=thefind.func.create({tag:'div',classname:'tf_combobox',append:select.parentNode,before:select});label=this.label=thefind.func.create({tag:'div',classname:'tf_combobox_label',append:combobox});button=this.button=thefind.func.create({tag:'div',classname:'tf_combobox_button',append:combobox});img=thefind.func.create({tag:'div',classname:'tf_combobox_image',append:button});ul=this.ul=thefind.func.create({tag:'ul',classname:'tf_combobox_options',append:combobox});label.innerHTML=select.options[select.selectedIndex].innerHTML;for(var s=0;s<select.options.length;s++){option=select.options[s];li=thefind.func.create({tag:'li',classname:'tf_combobox_option',append:ul,attributes:{innerHTML:option.innerHTML}});options.push({li:li,label:option.innerHTML,value:option.value});}
this.options=options;this.actions=actions;this.ul.style.display='block';this.height=this.ul.offsetHeight;this.ul.style.display='none';thefind.func.bind(combobox,'click',this);select.parentNode.removeChild(select);}}
this.show=function(){this.visible=true;thefind.utils.addClass(this.button,'selected');$TF(this.ul).css({display:'block',height:0}).animate({height:this.height+'px'},150,"easein");}
this.hide=function(){this.visible=false;thefind.utils.removeClass(this.button,'selected');(function(self){$TF(self.ul).animate({height:0},200,"easeout",function(){self.ul.style.display='none';});})(this);}
this.toggle=function(target){this.visible?this.hide():this.show();if(target.nodeName=='LI')
this.callback(target,this);}
this.handleEvent=function(event){var type=event.type||window.event,target=event.target||event.srcElement;switch(type){case'click':this.toggle(target);break;case'mouseover':break;case'mouseout':break;}}
this.init();});thefind.add('set_body',new function(){return;var b=document.getElementsByTagName('BODY')[0],c='tf_'+thefind.browser.type,c=c+' tf_'+thefind.browser.type+'_'+thefind.browser.version;thefind.utils.addClass(b,c);});if(!this.JSON){JSON=function(){function f(n){return n<10?'0'+n:n;}Date.prototype.toJSON=function(key){return this.getUTCFullYear()+'-'+f(this.getUTCMonth()+1)+'-'+f(this.getUTCDate())+'T'+f(this.getUTCHours())+':'+f(this.getUTCMinutes())+':'+f(this.getUTCSeconds())+'Z';};var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapeable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={'\b':'\\b','\t':'\\t','\n':'\\n','\f':'\\f','\r':'\\r','"':'\\"','\\':'\\\\'},rep;function quote(string){escapeable.lastIndex=0;return escapeable.test(string)?'"'+string.replace(escapeable,function(a){var c=meta[a];if(typeof c==='string'){return c;}return'\\u'+('0000'+(+(a.charCodeAt(0))).toString(16)).slice(-4);})+'"':'"'+string+'"';}function str(key,holder){var i,k,v,length,mind=gap,partial,value=holder[key];if(value&&typeof value==='object'&&typeof value.toJSON==='function'){value=value.toJSON(key);}if(typeof rep==='function'){value=rep.call(holder,key,value);}switch(typeof value){case'string':return quote(value);case'number':return isFinite(value)?String(value):'null';case'boolean':case'null':return String(value);case'object':if(!value){return'null';}gap+=indent;partial=[];if(typeof value.length==='number'&&!(value.propertyIsEnumerable('length'))){length=value.length;for(i=0;i<length;i+=1){partial[i]=str(i,value)||'null';}v=partial.length===0?'[]':gap?'[\n'+gap+partial.join(',\n'+gap)+'\n'+mind+']':'['+partial.join(',')+']';gap=mind;return v;}if(rep&&typeof rep==='object'){length=rep.length;for(i=0;i<length;i+=1){k=rep[i];if(typeof k==='string'){v=str(k,value,rep);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}else{for(k in value){if(Object.hasOwnProperty.call(value,k)){v=str(k,value,rep);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}v=partial.length===0?'{}':gap?'{\n'+gap+partial.join(',\n'+gap)+'\n'+mind+'}':'{'+partial.join(',')+'}';gap=mind;return v;}}return{stringify:function(value,replacer,space){var i;gap='';indent='';if(typeof space==='number'){for(i=0;i<space;i+=1){indent+=' ';}}else if(typeof space==='string'){indent=space;}rep=replacer;if(replacer&&typeof replacer!=='function'&&(typeof replacer!=='object'||typeof replacer.length!=='number')){throw new Error('JSON.stringify');}return str('',{'':value});},parse:function(text,reviver){var j;function walk(holder,key){var k,v,value=holder[key];if(value&&typeof value==='object'){for(k in value){if(Object.hasOwnProperty.call(value,k)){v=walk(value,k);if(v!==undefined){value[k]=v;}else{delete value[k];}}}}return reviver.call(holder,key,value);}cx.lastIndex=0;if(cx.test(text)){text=text.replace(cx,function(a){return'\\u'+('0000'+(+(a.charCodeAt(0))).toString(16)).slice(-4);});}if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,'@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,']').replace(/(?:^|:|,)(?:\s*\[)+/g,''))){j=eval('('+text+')');return typeof reviver==='function'?walk({'':j},''):j;}throw new SyntaxError('JSON.parse');}};}();}
thefind.add('JSON',new function(){this.parse=function(text){return this.JSON(['decode','parse'],text);},this.stringify=function(text){return this.JSON(['encode','stringify'],text);},this.JSON=function(parms,text){var key=typeof JSON[parms[0]]=='function'?parms[0]:parms[1];return JSON[key](text);}});thefind.add('ie6_purge',function(d){if(!thefind.ie6||!d)
return;var a=d.attributes,i,l,n;if(a){l=a.length;for(i=0;i<l;i+=1){n=a[i].name;if(typeof d[n]==='function')
d[n]=null;}}
a=d.childNodes;if(a){l=a.length;for(i=0;i<l;i+=1)
thefind.func.ie6_purge(d.childNodes[i]);}
CollectGarbage();});
ajaxlib=new function(){this.Queue=function(obj){if(obj.constructor.toString().indexOf("Array")!=-1){for(var i=0;i<obj.length;i++){if(!obj[i].method)obj[i].method="GET";this.urlqueue.push(obj[i]);}}else{if(!obj.method)obj.method="GET";this.urlqueue.push(obj);}
if(this.xmlhttpReady())
this.Go();}
this.Get=function(url,params,args){var req=this.parseURL(url);this.ProcessRequest(req,args);}
this.Post=function(form,params,args){var req=this.parseForm(form);this.ProcessRequest(req,args);}
this.ProcessRequest=function(req,args){if(typeof args!='undefined'){req.history=args.history||false;if(args.callback)
req.callback=args.callback;if(args.failurecallback)
req.failurecallback=args.failurecallback;if(args.timeout)
req.timeout=args.timeout;if(args.timeoutcallback)
req.timeoutcallback=args.timeoutcallback;}
this.Queue(req);}
this.Go=function(){if(this.urlqueue.length>0){obj=this.urlqueue.shift();if(!this._go(obj))
this.urlqueue.unshift(obj);}}
this.parseURL=function(turl){var ret=new Object();ret.method="GET";var url=new String(turl);if(url.indexOf("?")>0){ret.url=url.substr(0,url.indexOf("?"));ret.args=url.substr(url.indexOf("?")+1);}else{ret.url=url;ret.args="";}
return ret;}
this.parseForm=function(form){var ret=new Object();ret.method=(form.getAttribute("method")?form.getAttribute("method").toUpperCase():"GET");ret.url=form.getAttribute("action");ret.args="";for(var i=0;i<form.elements.length;i++){element=form.elements[i];var name=new String(element.name);if(name.length>0&&name!="undefined"&&element.value!="undefined"&&!element.disabled){if(element.type=="checkbox"){ret.args+="&"+escape(name)+"="+(element.checked?(element.getAttribute("value")?escape(element.value):1):0);}else if(element.type=="radio"){if(element.checked){ret.args+="&"+escape(name)+"="+escape(element.value);}}else{ret.args+="&"+escape(name)+"="+escape(element.value).replace(/\+/g,"%2B");}}}
return ret;}
this.xmlhttpReady=function(){if(this.xmlhttp.readyState>0&&this.xmlhttp.readyState<4){return false;}
return true;}
this.blah=function(responses){var common={inlinescripts:[],data:{},dependencies:{}};for(var i=0;i<responses.length;i++){if(typeof this.responsehandlers[responses[i]['type']]=='function')
this.responsehandlers[responses[i]['type']](responses[i],common);else
console.log('No handler for type '+responses[i]['type']);}
var cssparms='',javascriptparms='';for(var key in common.dependencies.css){if(common.dependencies.css.hasOwnProperty(key)){if(common.dependencies.css[key].length>0)
cssparms+=key+'='+common.dependencies.css[key].join('+')+'&';}}
for(var key in common.dependencies.javascript){if(common.dependencies.javascript.hasOwnProperty(key)){if(common.dependencies.javascript[key].length>0)
javascriptparms+=key+'='+common.dependencies.javascript[key].join('+')+'&';}}
var batch=new thefind.func.dependencies_batch();if(cssparms.length>0)
batch.add('/css/main?'+cssparms.substr(0,cssparms.length-1),'css');if(javascriptparms.length>0)
batch.add('/scripts/main?'+javascriptparms.substr(0,javascriptparms.length-1),null,true);if(typeof obj!='undefined'&&obj.callback){try{ajaxlib.executeCallback(obj.callback,common.data);}catch(e){batch.callback(function(){ajaxlib.executeCallback(obj.callback,common.data);});}}}
this.responsehandlers={'xhtml':function(response,common){if(response['target']&&response['_content']){var targetel=document.getElementById(response['target']);if(targetel){if(response['append']==1||response['append']=='true')
targetel.innerHTML+=response['_content'];else{thefind.func.ie6_purge(targetel);targetel.innerHTML=response['_content'];}}}},'javascript':function(response,common){if(response['_content'])
common.inlinescripts.push(response['_content']);},'data':function(response,common){if(response['name']&&response['_content']){common.data[response['name']]=thefind.JSON.parse(response['_content']);}},'dependency':function(response,common){if(response['deptype']=='component'&&response['name']){var name=response['name'].split('.',2);if(name[0]&&response['subtypes']){var subtypes=response['subtypes'].split(',');for(var i=0;i<subtypes.length;i++){if(!common.dependencies[subtypes[i]])
common.dependencies[subtypes[i]]=[];if(!common.dependencies[subtypes[i]][name[0]])
common.dependencies[subtypes[i]][name[0]]=[];common.dependencies[subtypes[i]][name[0]].push(name[1]);}}}},'debug':function(response,common){if(response['_content']){var debugcontainer=document.getElementById('tf_debug_tab_logger');if(debugcontainer){thefind.func.ie6_purge(debugcontainer);debugcontainer.innerHTML+=response['_content'];}
if(typeof tf_debugconsole!='undefined')
tf_debugconsole.scrollToBottom();}}}
this.processResponse=function(dom,docroot,obj,ignore){if((typeof thefind!='undefined'&&typeof thefind.ajax_back_button!='undefined')&&(typeof search!='undefined'&&search.urlhash)&&(typeof obj!='undefined'&&obj.url=='')&&(!ignore)){thefind.ajax_back_button.add(dom,docroot,obj);}
if(dom==null)
return;var batch=new thefind.func.dependencies_batch(),inlinescripts=[],components={},data={},css={},js={},inc=1;for(var i=0;i<dom.childNodes.length;i++){var res=dom.childNodes.item(i);if(res.nodeType==1){var typeattr=res.attributes.getNamedItem("type"),type="xhtml";if(typeattr)
type=typeattr.nodeValue;if(type=="xhtml"){var targetattr=res.attributes.getNamedItem("target");if(targetattr){var target=targetattr.nodeValue,append=res.attributes.getNamedItem("append"),fchild=res.firstChild,content=fchild?fchild.nodeValue:false,element=docroot.getElementById(target);if(element&&typeof content=='string'){if(append&&(append.nodeValue==1||append.nodeValue=="true")){element.innerHTML+=content;}else{element.innerHTML=content;}
var scripts=element.getElementsByTagName("SCRIPT");if(scripts.length>0){for(var j=0;j<scripts.length;j++){if(typeof scripts[j].text=='string'){var text=scripts[j].text;inlinescripts.push(text);}else if(scripts[j].src){}}}
var infobox=thefind.infobox.target(element);if(infobox&&infobox.args.reposition)
inlinescripts.push("thefind.infobox.position('"+infobox.name+"', true);");}}}else if(type=="javascript"){var content=res.firstChild.nodeValue;inlinescripts.push(content);}else if(type=="data"){var nameattr=res.attributes.getNamedItem("name"),content=res.firstChild;if(nameattr&&content){if(nameattr.nodeValue=='infobox.content'){var text=thefind.JSON.parse(content.nodeValue),div=document.createElement('div');thefind.func.ie6_purge(div);div.innerHTML=text;$TF("script",div).each(function(k,v){inlinescripts.push(v.innerHTML);});var infobox=thefind.infobox.current;if(infobox&&infobox.args.reposition)
inlinescripts.push("thefind.infobox.position('"+infobox.name+"', true);");}
data[nameattr.nodeValue]=thefind.JSON.parse(content.nodeValue);}}else if(type=="debug"){var content=res.firstChild.nodeValue,debugcontainer=document.getElementById('tf_debug_tab_logger');if(debugcontainer)
debugcontainer.innerHTML+=content;if(typeof tf_debugconsole!='undefined')
tf_debugconsole.scrollToBottom();}else if(type=="dependency"){var deptype=thefind.utils.isNull(res.attributes)?'':res.attributes.getNamedItem("deptype").nodeValue;switch(deptype){case'javascript':var url=thefind.utils.isNull(res.attributes)?'':res.attributes.getNamedItem("url").nodeValue;batch.add(url+'&async=2');break;case'component':var name=thefind.utils.isNull(res.attributes)?'':res.attributes.getNamedItem("name").nodeValue.split('.');var subtypes=thefind.utils.isNull(res.attributes)?false:res.attributes.getNamedItem("subtypes").nodeValue;if(name[0]&&subtypes){if(typeof name[1]=='undefined')
name[1]=name[0];subtypes=subtypes.split(',');for(var typenum=0;typenum<subtypes.length;typenum++){if(!components[subtypes[typenum]])
components[subtypes[typenum]]=[];if(!components[subtypes[typenum]][name[0]])
components[subtypes[typenum]][name[0]]=[];components[subtypes[typenum]][name[0]].push(name[1]);}}
break;case'placemark':break;case'css':break;}}}}
var parms='',key,cssparms='',javascriptparms='',delim='?';for(var key in components.css){if(components.css.hasOwnProperty(key)){if(components.css[key].length>0)
cssparms+=delim+key+'='+components.css[key].join('+');delim='&';}}
delim='?';for(var key in components.javascript){if(components.javascript.hasOwnProperty(key)){if(components.javascript[key].length>0)
javascriptparms+=delim+key+'='+components.javascript[key].join('+');delim='&';}}
if(cssparms.length>0)
batch.add('/css/main'+cssparms,'css');if(javascriptparms.length>0)
batch.add('/scripts/main'+javascriptparms,null,true);var execute_scripts=function(){if(inlinescripts.length>0){var script_text='';for(var i=0;i<inlinescripts.length;i++){if(!inlinescripts[i]||typeof inlinescripts[i]=='undefined')
continue;else
script_text+=inlinescripts[i]+'\n';}
try{eval(script_text);}catch(e){batch.callback(script_text);}}}
if(nameattr&&nameattr.nodeValue=='infobox.content')
setTimeout(execute_scripts,1);else
execute_scripts();if(obj&&obj.callback)
ajaxlib.executeCallback(obj.callback,data);}
this._go=function(obj){var docroot=this.docroot;var xmlhttp=this.xmlhttp;var processResponse=this.processResponse;var timeouttimer=false;if(obj.history){this.setHistory(obj.args);if(this.iframe){this.iframe.src="/ajax-blank.htm?"+obj.args+"#"+obj.url;return;}}
if(!obj.cache){obj.args=(obj.args&&obj.args.length>0?obj.args+"&":"")+"_ajaxlibreqid="+(parseInt(new Date().getTime().toString().substring(0,10))+parseFloat(Math.random()));}
if(obj.timeout&&obj.timeoutcallback){timeouttimer=window.setTimeout(function(){obj.failurecallback=false;xmlhttp.abort();obj.timeoutcallback();},obj.timeout||5000);}
readystatechange=function(){if(xmlhttp.readyState==4){if(timeouttimer)
window.clearTimeout(timeouttimer);if(xmlhttp.status==200){if(xmlhttp.responseXML){var dom=xmlhttp.responseXML.firstChild;processResponse(dom,docroot,obj);}else if(xmlhttp.responseText){if(obj.callback){ajaxlib.executeCallback(obj.callback,xmlhttp.responseText);}}}else{if(obj.failurecallback){ajaxlib.executeCallback(obj.failurecallback);}}
setTimeout('ajaxlib.Go()',0);}}
try{if(obj.method=="POST"){xmlhttp.open(obj.method,obj.url,true);xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded');xmlhttp.setRequestHeader("X-Ajax","1");xmlhttp.onreadystatechange=readystatechange;xmlhttp.send(obj.args);}else if(obj.method=="GET"){xmlhttp.open(obj.method,obj.url+"?"+obj.args,true);xmlhttp.setRequestHeader("X-Ajax","1");xmlhttp.onreadystatechange=readystatechange;xmlhttp.send(null);}else if(obj.method=="SCRIPT"){var url=this.host+obj.url;if(obj.args)url+='?'+thefind.utils.encodeURLParams(obj.args);thefind.file_utils.get('javascript',url);}}catch(e){if(obj.failurecallback){ajaxlib.executeCallback(obj.failurecallback,e);}
return false;}
return true;}
this.setHistory=function(hash){this.docroot.location.hash=hash;this.lasthash=this.docroot.location.hash;}
this.checkHistory=function(){if(this.docroot.location.hash!=this.lasthash){this.processHash(this.docroot.location.hash);this.lasthash=this.docroot.location.hash;}}
this.processHash=function(hash){return false;url=String(document.location);if(hash.length>0)
if(url.indexOf("#")>0)
this.Get(url.substr(0,url.indexOf("#"))+"?"+hash.substr(1));else
this.Get(url+"?"+hash.substr(1));else
this.Get(url.replace("#","?"));}
this.setLoader=function(target,img,text){if(!text)text="";if(e=document.getElementById(target)){thefind.func.ie6_purge(e);e.innerHTML='<div style="text-align: center;">'+text+'<img src="'+img+'" alt="Loading..." /></div>';}}
this.getHTTPObject=function(){if(!this.xmlhttp){var xmlhttp=false;if(typeof ActiveXObject!='undefined'){try{xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}catch(E){xmlhttp=false;}}}
if(!xmlhttp&&typeof XMLHttpRequest!="undefined"){try{xmlhttp=new XMLHttpRequest();}catch(e){xmlhttp=false;}}
this.xmlhttp=xmlhttp;}
return this.xmlhttp;}
this.getIFRAMEObject=function(iframeID){if(!this.iframe){return;var iframe,iframeDocument;if(document.createElement){try{var tempIFrame=document.createElement('iframe');tempIFrame.setAttribute('id',iframeID);tempIFrame.style.border='0px';tempIFrame.style.width='0px';tempIFrame.style.height='0px';iframe=document.body.appendChild(tempIFrame);if(document.frames){iframe=document.frames[iframeID];}}catch(ex){var iframeHTML='\<iframe id="'+iframeID+'"';iframeHTML+=' style="border:0px; width:0px; height:0px;"';iframeHTML+='><\/iframe>';document.body.innerHTML+=iframeHTML;iframe=new Object();iframe.document=new Object();iframe.document.location=new Object();iframe.document.location.iframe=document.getElementById(iframeID);iframe.document.location.replace=function(location){this.iframe.src=location;}}}
this.iframe=document.getElementById(iframeID);}
return this.iframe;}
this.executeCallback=function(){var args=[];for(var i=0;i<arguments.length;i++)
args[i]=arguments[i];var callback=args.shift();if(callback){if(callback.constructor.toString().indexOf("Array")!=-1&&callback.length==2){callback[1].apply(callback[0],args);}else{callback.apply(this,args);}}}
this.getHTTPObject();if(thefind.browser.type=="msie"){this.getIFRAMEObject("hiddeniframe");}
this.lasthash="";this.urlqueue=new Array();this.docroot=document;this.host=document.location.protocol+'//'+document.location.host;}
function ajaxChild(url){var qstr=url.substr(url.indexOf("?")+1,(url.indexOf("#")-url.indexOf("?")-1));var file=url.substr(url.indexOf("#")+1);if(file.length>0&&qstr.length>0){if(parent.ajaxlib){parent.ajaxlib.Get(file+"?"+qstr);}else{setTimeout('parent.ajaxlib.Get("'+file+'?'+qstr+'")',100);}}}
function ajaxLink(ajaxlib,link,history){ajaxlib.Get(link,history);return false;}
function ajaxForm(ajaxlib,form,history){ajaxlib.Post(form,history);return false;}
thefind.add("upfront",new function(){this.initialized={};this.elements=[];this.init=function(classname,claimid){this.classname=classname;var elements=thefind.utils.getElementsByClassName(document,"A",this.classname);if(elements.length>0){this.setResourceHostname(elements[0].href);(function(self,claimid){for(var i=0;i<elements.length;i++){if(!self.elementInitialized(elements[i])){self.elements.push(elements[i]);thefind.func.bind(elements[i],'click',function(ev){if(typeof thefind.infobox=='undefined'||!self.initialized[claimid]){ajaxlib.Queue({method:"SCRIPT",url:"/upfront/badge.json",args:"claimid="+claimid,callback:function(html){if(typeof html['infobox.content']!='undefined')
html=html['infobox.content'];self.initPopup(claimid,html);thefind.infobox.show("upfront_badge_"+claimid);}});}else{thefind.infobox.show("upfront_badge_"+claimid);}
ev.preventDefault();return false;});}}})(this,claimid);}}
this.setResourceHostname=function(url){var re=new RegExp('^(http(?:s)?\://)([^/]+)','i');var scheme=(document.location.protocol||"http:")+"//";var hostname="upfront.thefind.com";var m=url.match(re);if(m){if(m[2]&&m[2]!="www.thefind.com")
hostname=m[2].toString();}
ajaxlib.host=scheme+hostname;thefind.dependencies.host=scheme+hostname;}
this.initPopup=function(claimid,content){if(!this.initialized[claimid]){if(thefind.tplmgr&&thefind.infobox){this.initialized[claimid]=true;thefind.tplmgr.Create('upfront.badge.border','<div id="tf_upfront_badge">(%$content)</div>');thefind.tplmgr.Create('upfront.badge.closebutton','<div class="tf_upfront_badge_closebutton"><a href="#" class="tf_utils_infobox_close" onclick="thefind.infobox.hide(\'(%$name)\'); return false">X</a></div>');thefind.tplmgr.Create('upfront.badge.content.'+claimid,content);thefind.infobox.add("upfront_badge_"+claimid,{ajax:false,event:'click',absolute:true,center:true,reposition:true,resizeclose:false,lightbox:false,width:'30em',border:'upfront.badge.border',titlebar:'upfront.badge.closebutton',content:'upfront.badge.content.'+claimid,scollTop:true,tail:false,bgcolor:'#fff',font:'Arial, sans-serif',ajaxmethod:'SCRIPT'},{claimid:claimid},'upfront.badge.content.'+claimid);}}}
this.elementInitialized=function(element){for(var i=0;i<this.elements.length;i++){if(this.elements[i]==element)
return true;}
return false;}});var scripts=document.getElementsByTagName("script");for(var i=0;i<scripts.length;i++){if(scripts[i].src.indexOf('upfront-badgeinit')!=-1){var src=scripts[i].innerHTML;if(src.length>0)
eval(src);break;}}
if(typeof thefind!='undefined')thefind.dependencies.registerMany({"utils":["init","ajaxlib"],"upfront":["badgeinit"]});