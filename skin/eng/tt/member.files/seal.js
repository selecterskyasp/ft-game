//Parse Query String 
function parseQuery ( query ) 
{
    var Params = new Object ();
    if ( ! query ) return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for ( var i = 0; i < Pairs.length; i++ ) 
    {
        var KeyVal = Pairs[i].split('=');
        if ( ! KeyVal || KeyVal.length != 2 ) continue;
        var key = unescape( KeyVal[0] );
        var val = unescape( KeyVal[1] );
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
};

var scripts = document.getElementsByTagName('script');
var myScript = scripts[ scripts.length - 1 ];
var queryString = myScript.src.replace(/^[^\?]+\??/,'');
var params = null;
params = parseQuery( queryString );

var holdOpen = false;

function Popup(image, container)
{
    var elem = document.getElementById(container);
    if (elem == undefined) return;
    
    elem.style.visibility="visible";
    elem.visibility="visible";
    var cert = [302, 356];    
        
    var scroll = Scroll(); 
    var pos = Pos(image);
    
    var screen = Screen();  
    var imagesize = ImageSize(image.src);
             
    elem.style.width = cert[0] + 'px';    
	elem.style.height = cert[1] + 'px';
	var coord = Adjust(screen, cert, pos, imagesize, scroll);
	
    elem.style.left = coord[0] + 'px';
	elem.style.top = coord[1] + 'px';	
};

function Adjust(screen, certificate, location, image, scroll)
{
    var i;
    var coord = new Array();
    for (i=0; i<2; i++)
    {
        coord[i] = location[i] - scroll[i];
    
        if (screen[i] < certificate[i])
	    {
	        coord[i] = 0;
	    }
        else
        {        
            coord[i] = (coord[i] - certificate[i] > 0) 
                ? coord[i] - certificate[i] : coord[i] + image[i];
            if ((coord[i] + certificate[i]) > screen[i])
                coord[i] = screen[i] - certificate[i];
        }
        
        coord[i] += scroll[i];
        if (location[i+2] != null)
        {
            coord[i] += (location[i+2] - location[i]);
        }
    }
    return coord;
};

function ImageSize(img)
{
    if (img.indexOf('header') >= 0)
    {
        w_img = 150;
        h_img = 42;
    }
    else if (img.indexOf('small') >= 0)
    {
        w_img = 100;
        h_img = 64;
    }
    else
    {
        w_img = 120;
        h_img = 77;
    }
    return [w_img, h_img];
}

function Scroll()
{    
    var myScrollX = document.body.scrollLeft;
    var myScrollY = document.body.scrollTop;
    
    if (myScrollX == 0)
    {
        if (window.pageXOffset)
            myScrollX = window.pageXOffset;
        else
            myScrollX = (document.body.parentElement) ? document.body.parentElement.scrollLeft : 0;
    }
    if (myScrollY == 0)
    {
        if (window.pageYOffset)
            myScrollY = window.pageYOffset;
        else
            myScrollY = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
    }
    return [myScrollX, myScrollY];
}

function Pos(obj)
{
    var curleft = 0;
    var curtop = 0;
    var relleft = null;
    var reltop = null;
    if(obj.offsetParent)
    while(1) 
    {        
        curleft += obj.offsetLeft;
        curtop += obj.offsetTop;
        if(!obj.offsetParent)
            break;
        obj = obj.offsetParent;
        if (obj.currentStyle != undefined && obj.currentStyle.position != undefined 
            && obj.currentStyle.position != 'static'
            && relleft == null && reltop == null)
        {
            relleft = curleft;
            reltop = curtop;
        }
    }
    else if(obj.x && obj.y)
    {
        curleft += obj.x;
        curtop += obj.y;
    }
    
    return [curleft, curtop, relleft, reltop];
}

function Screen()
{
    //Height
    var myWidth = 0, myHeight = 0;
    if( typeof( window.innerWidth ) == 'number' ) 
    {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
    } 
    else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) 
    {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
    } 
    else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) 
    {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
    }
    return [myWidth, myHeight];
}

function Hide(container,delay)
{
    if (delay == null) delay = 0;
    
    seconds=parseInt(delay);
    if (holdOpen)
    {
	    timerID=setTimeout("Hide('" + container + "'," + seconds + ")",1000);  
	} 
	else if(seconds>0) 
    {
        seconds--;
	    timerID=setTimeout("Hide('" + container + "'," + seconds + ")",1000);
    }
    else
    {
	    var elem = document.getElementById(container);
	    if (elem != undefined)
	    {
            elem.innerHtml = "";
            elem.style.visibility="hidden";
        }
    } 
};

function OpenCertificate(link)
{
    var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no';
    window.open(link.href,'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no');
    return false;
};

function SetSeals(name, path)
{
    var links = document.getElementsByName('trustlink');
    var i;
    for (i=0; i< links.length; i++)
    {
        if (links[i].onclick == null)
        links[i].onclick = function(){ return OpenCertificate(this); };        
    }
    
    var seals = document.getElementsByName('trustseal');
    for (i=0; i< seals.length; i++)
    {
        if (seals[i].onmouseout == null)
        {
            seals[i].oncontextmenu = function() {
                alert("Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 2007.");
                return false; };
                
            seals[i].onmouseover = function() { holdOpen = true; Popup(this,'viewer'); };
            seals[i].onmouseout = function() { holdOpen = false; Hide('viewer',1); };
                        
            if (i == 0) //Only write this once
            {
                if (path != '')
                {
                    document.write("<iframe id='viewer' frameborder='no' style='visibility:hidden;position:absolute;width:0;height:0;z-index:1000000;overflow:hidden;background-color:Transparent;' "
                    + "src='" + path + "certificates/pages/mini-" + name + ".html' "
                    + "onmouseover='holdOpen = true;' onmouseout='holdOpen = false;' ALLOWTRANSPARENCY='true'></iframe>");
                }
                else
                {
                    //document.write("<iframe id='viewer' frameborder='no' style='visibility:hidden;position:absolute;width:0;height:0;z-index:1000000;overflow:hidden;background-color:Transparent;' "
                    //+ "src='" + path + "certificates/pages/mini.html' "
                    //+ "onmouseover='holdOpen = true;' onmouseout='holdOpen = false;' ALLOWTRANSPARENCY='true'></iframe>"); 
                    document.write("<iframe id='viewer' frameborder='no' style='visibility:hidden;position:absolute;width:0;height:0;z-index:1000000;overflow:hidden;background-color:Transparent;' "
                    + "src='" + path + "certificates/mini_certificate.php?site=" + name + "' "
                    + "onmouseover='holdOpen = true;' onmouseout='holdOpen = false;' ALLOWTRANSPARENCY='true'></iframe>");
                }
            }
        }
    }
    
    //Goofy workaround so some don't popup
    var nopopupseals = document.getElementsByName('trust-seal');
    for (i=0; i< nopopupseals.length; i++)
    {
        if (nopopupseals[i].oncontextmenu == null)
        {
            nopopupseals[i].oncontextmenu = function() {
                alert("Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 2007.");
                return false; };
        }
    }
};

function SetVideo(name, id, path, pagetype, mini)
{    
    if (video != undefined)
    {
        //pagetype = (pagetype == 'demo') ? '/demo' : '';
        var container = document.getElementById('iSpeakVideo_Container');
        var adjy = (video.isFirefox) ? '-30px' : '-25px';
        var x_adj = 0;
        if 
        (pagetype == '' && 
            (
                //(id ==  772) || // && !video.isFirefox && !video.isNetscape) ||
                (id ==  785) ||
                (id == 1248) ||
                (id ==  706) ||
                (id == 1284) ||
                (id == 1407) ||
                (id == 1524) ||
                (id == 1051) ||
                (id == 1665) ||
                (id == 1126) ||
                (id == 2070) ||
                (id == 2066) ||
                (id == 937)              
            )           
        )
        x_adj = -27;
        else if 
        (pagetype == '' && 
            (
                (id == 816) ||
                (id == 817) ||
                (id == 1696)                                      
            )           
        )
        x_adj = -50;
        if (mini) x_adj = x_adj / 2;
        var adjx = (video.isFirefox) ? 27 : 29;       
        adjx += x_adj;
        
        var size;
        var width;
        
        if (mini)
        {
            size = 'mini';
            width = '100px';
            adjx -= 14;    
        }
        else
        {
            size = 'header';
            width = '150px';    
        }
        
        var link = (pagetype == 'demo') 
            ? path + 'seals/trust-seal.php?id=' + id + '&seal=security&size=' + size + '&pagetype=demo' 
            : path + 'seals/' + id + '/security/' + size;
        /*container.innerHTML += '<a name="trustlink" href="' + path + 'certificates/' + name + '" target="_blank">'
            + '<img id="video-seal" style="z-index:1000000; border:0px; position:relative; left:' + adjx + 'px; top:' 
            + adjy + '; width:' + width + ';" name="trust-seal" alt="security certified" src="' + path + 'seals/security/' 
            + id + '-' + size + '.gif"/></a>';*/
        container.innerHTML += '<a name="trustlink" href="' + path + 'certificates/' + name + '" target="_blank">'
            + '<img id="video-seal" style="z-index:1000000; border:0px; position:relative; left:' + adjx + 'px; top:' 
            + adjy + '; width:' + width + ';" name="trust-seal" alt="security certified" src="' + link + '"/></a>';
            //' + path + 'seals/' 
            //+ id + '/security/' + size + pagetype + '"/></a>';
    }
}

var path = (params['pagetype'] != 'demo') ? 'https://secure.trust-guard.com/' : '';
if (mini == undefined) var mini = false;

if (params['video'] == 'true')
{
    SetVideo(params['site'], params['id'], path, params['pagetype'], mini);
}

SetSeals(params['site'], path);


//Test to move seals around

//        container.innerHTML += '<a name="trustlink" href="' + path + 'certificates/' + name + '" target="_blank">'
//        + '<img id="video-seal" style="z-index:1000000; border:0px; ';
//        if (typeof y != 'undefined')
//        {
//            if (typeof drawseal != 'undefined')
//            {
//                //var style = 'style="z-index:1000000; border:0px; position:relative; left:' + adjx + 'px; top:' 
//                //    + adjy + '; width:150px;"';
//                //var seal = document.getElementById('video-seal'); 
//                //seal.style.left = adjx + 'px';
//                //seal.style.top = adjy;
//            }
//            else
//            {
//                container.innerHTML += '<a name="trustlink" href="' + path + 'certificates/' + name + '" target="_blank">'
//                + '<img id="video-seal" style="z-index:1000000; border:0px; position:relative; left:' + adjx + 'px; top:' 
//                + adjy + '; width:150px;" name="trust-seal" alt="security certified" src="' + path + 'seals/' 
//                + id + '/security/header' + pagetype + '"/></a>';
//            }
//        }
//        else
//        {
//            document.write('<a name="trustlink" href="' + path + 'certificates/' + name + '" target="_blank">'
//            + '<img id="video-seal" style="z-index:1000000; border:0px; width:150px;" name="trust-seal" alt="security certified" src="' + path + 'seals/' 
//            + id + '/security/header' + pagetype + '"/></a>');
//        }